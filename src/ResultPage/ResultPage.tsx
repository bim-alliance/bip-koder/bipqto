import { useResizable } from "react-resizable-layout"
import InstanceTable from "./Tables/InstanceTable"
import Viewer3d from "./Viewer3d"
import { useAppDispatch, useAppSelector } from "../app/hooks"
import {
  rSetActiveExtractionConfig,
  rSetProfileOrBipProperties,
  rSetSelectedGlobalId,
  rSetShowInvalid,
  rSetShowValid,
  rSetTableMode,
  selectAgGridGroupColumns,
  selectRowData,
  selectStats,
} from "../bimSlice"
import { Button, Group, Title } from "@mantine/core"
import { LayoutComboBox } from "../LayoutComboBox"
import { GroupTable } from "./Tables/GroupTable"
import { EXTRACTION_CONFIGS } from "../extractionConfigs"
import { exportExcel } from "../utils"
import SampleSeparator from "./ResizeableLayoutDemo/SampleSeparator"
import "./ResizeableLayoutDemo.css"
import DetailItem from "./DetailItem"

const ResultPage: React.FC = () => {
  const selectTableMode = useAppSelector(state => state.bim.tableMode)

  const dispatch = useAppDispatch()

  // Footer Resizer
  const footerResizer = useResizable({
    axis: "y",
    initial: 500,
    min: 50,
    reverse: true,
  })

  // Left Resizer
  const leftResizer = useResizable({
    axis: "x",
    initial: 250,
    min: 50,
  })

  // Right Resizer
  const rightResizer = useResizable({
    axis: "x",
    initial: 500,
    min: 50,
    reverse: true,
  })

  return (
    <div className="flex flex-column h-screen overflow-hidden w-100">
      <div style={{ height: "50px" }}>
        <ButtonBar />
      </div>
      <div className="flex grow">
        <SampleSeparator
          isDragging={leftResizer.isDragging}
          {...leftResizer.separatorProps}
        />
        <div className="flex grow">
          <div className="grow bg-darker contents">
            {selectTableMode === "INSTANCE" && <InstanceTable />}
            {selectTableMode === "GROUPED" && <GroupTable />}
          </div>
          <SampleSeparator
            isDragging={rightResizer.isDragging}
            {...rightResizer.separatorProps}
          />
          <div
            className={joinClassNames(rightResizer.isDragging && "dragging")}
            style={{ width: rightResizer.position }}
          >
            <DetailItem />
          </div>
        </div>
      </div>
      <SampleSeparator
        dir="horizontal"
        isDragging={footerResizer.isDragging}
        {...footerResizer.separatorProps}
      />
      <div
        className={joinClassNames(
          "shrink-0 bg-darker contents",
          footerResizer.isDragging && "dragging",
        )}
        style={{ height: footerResizer.position }}
      >
        <Viewer3d
          onClickOnItem={globalIds =>
            dispatch(rSetSelectedGlobalId(globalIds[0]))
          }
          performanceThreshold={2}
        />
      </div>
    </div>
  )
}

export default ResultPage

const ButtonBar: React.FC = () => {
  const dispatch = useAppDispatch()

  const selectActiveExtractionConfig = useAppSelector(
    state => state.bim.activeExtractionConfig,
  )

  const selectShowValid = useAppSelector(state => state.bim.showValid)
  const selectShowInvalid = useAppSelector(state => state.bim.showInvalid)

  const selectTableMode = useAppSelector(state => state.bim.tableMode)

  const extractConfig = EXTRACTION_CONFIGS.find(
    config => config.name === selectActiveExtractionConfig,
  )

  const selectProfileOrBipProperties = useAppSelector(
    state => state.bim.profileOrBipProperties,
  )

  const stats = useAppSelector(selectStats)

  const agGridInstanceColumns = useAppSelector(state => state.bim.tableColumns)
  const agGridGrouColumns = useAppSelector(selectAgGridGroupColumns)
  const tableMode = useAppSelector(state => state.bim.tableMode)

  const rowData = useAppSelector(selectRowData)

  return (
    <Group style={{ paddingTop: "5px", paddingBottom: "5px" }}>
      <Button.Group>
        <Button
          color="blue"
          onClick={() => dispatch(rSetProfileOrBipProperties("PROFILE"))}
          variant={
            selectProfileOrBipProperties === "PROFILE" ? "filled" : "outline"
          }
        >
          Profil
        </Button>
        <Button
          color="blue"
          onClick={() => dispatch(rSetProfileOrBipProperties("BIP_PROPERTIES"))}
          variant={
            selectProfileOrBipProperties === "BIP_PROPERTIES"
              ? "filled"
              : "outline"
          }
        >
          Eget val
        </Button>
      </Button.Group>
      {selectProfileOrBipProperties === "PROFILE" && (
        <>
          <Title order={6}>Profil:</Title>
          <LayoutComboBox
            onChange={item => dispatch(rSetActiveExtractionConfig(item?.name))}
            options={EXTRACTION_CONFIGS}
            displayProperty="name"
            currentValue={extractConfig}
          />
          <Button.Group>
            <Button
              onClick={() => dispatch(rSetTableMode("INSTANCE"))}
              variant={selectTableMode === "INSTANCE" ? "filled" : "outline"}
            >
              Instanser
            </Button>
            <Button
              onClick={() => dispatch(rSetTableMode("GROUPED"))}
              variant={selectTableMode === "GROUPED" ? "filled" : "outline"}
            >
              Grupperat
            </Button>
          </Button.Group>
        </>
      )}
      {selectTableMode !== "GROUPED" && (
        <Button.Group>
          <Button
            color="red"
            onClick={() => dispatch(rSetShowInvalid(!selectShowInvalid))}
            variant={selectShowInvalid ? "filled" : "outline"}
          >
            Inkorrekta ({stats.invalid.percentage}%)
          </Button>
          <Button
            color="green"
            onClick={() => dispatch(rSetShowValid(!selectShowValid))}
            variant={selectShowValid ? "filled" : "outline"}
          >
            Korrekta ({stats.valid.percentage}%)
          </Button>
        </Button.Group>
      )}
      <Button
        onClick={() =>
          exportExcel(
            tableMode === "INSTANCE"
              ? agGridInstanceColumns
              : agGridGrouColumns,
            rowData,
          )
        }
      >
        Excel
      </Button>
    </Group>
  )
}

export const joinClassNames = (...args: any[]) => args.filter(Boolean).join(" "); //used for joining classnames
