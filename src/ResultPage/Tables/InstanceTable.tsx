import { useAppDispatch, useAppSelector } from "../../app/hooks"
import { AgGridReact } from "ag-grid-react" // React Data Grid Component
import "ag-grid-community/styles/ag-grid.css" // Mandatory CSS required by the Data Grid
import "ag-grid-community/styles/ag-theme-balham.css"
import { ICellRendererParams, RowClassParams } from "ag-grid-community"
import { rSetSelectedGlobalId, selectRowData } from "../../bimSlice"
import TableColumnsSelector from "../../TableColumnsSelector"
import { IBimItem } from "../../types"

import { IconCheck, IconCircleCheckFilled } from "@tabler/icons-react"

const InstanceTable: React.FC = () => {
  const dispatch = useAppDispatch()

  const selectGlobalId = useAppSelector(state => state.bim.selectedGlobalId)
  const agGridInstanceColumns = useAppSelector(state => state.bim.tableColumns)

  const rowData = useAppSelector(selectRowData)

  const getRowStyle = (params: RowClassParams) => {
    if (params.data.globalId === selectGlobalId) {
      return { background: "#3191e7" }
    }
  }

  const selectProfileOrBipProperties = useAppSelector(
    state => state.bim.profileOrBipProperties,
  )

  const columns = [
    {
      field: "zodErrors",
      hide: true,
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellRenderer: (params: ICellRendererParams<IBimItem>) => {
        const row = params.data
        return (
          <>
            {row?.zodErrors == null && <IconCheck style={{ color: "green" }} />}
            {row?.zodErrors != null && (
              <IconCircleCheckFilled style={{ color: "red" }} />
            )}
          </>
        )
      },
    },
    ...agGridInstanceColumns,
  ]
  return (
    <div
      style={{
        height: "100%",
        width: "100%",
        display: "flex",
        flexDirection: "column",
      }}
    >
      {selectProfileOrBipProperties === "BIP_PROPERTIES" && (
        <div style={{ height: "50px" }}>
          <TableColumnsSelector />
        </div>
      )}
      <div style={{ flex: 1 }} className="ag-theme-balham">
        <AgGridReact
          rowData={rowData}
          columnDefs={columns}
          getRowStyle={getRowStyle}
          onRowClicked={event => {
            dispatch(rSetSelectedGlobalId(event.data?.globalId))
          }}
        />
      </div>
    </div>
  )
}

export default InstanceTable;
