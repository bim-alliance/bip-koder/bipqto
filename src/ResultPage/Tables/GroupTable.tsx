import { useAppSelector } from '../../app/hooks';
import { selectAgGridGroupColumns, selectRowData } from '../../bimSlice';
import { AgGridReact } from 'ag-grid-react'; // React Data Grid Component
import "ag-grid-community/styles/ag-grid.css"; // Mandatory CSS required by the Data Grid
import "ag-grid-community/styles/ag-theme-balham.css";

export const GroupTable:React.FC = () => {
  const agGridGrouColumns = useAppSelector(selectAgGridGroupColumns);
  const rowData = useAppSelector(selectRowData);

  return <div
    style={{height: "100%", width: "100%"}}
    className="ag-theme-balham" // applying the Data Grid theme
  >
    <AgGridReact
      rowData={rowData}
      columnDefs={agGridGrouColumns}
    />
  </div>
}
