import React from "react"
import { useResizable } from "react-resizable-layout"
import SampleSeparator from "./SampleSeparator"
import "../ResizeableLayoutDemo.css"
import { joinClassNames } from "../ResultPage"

const ResizeableLayoutDemo: React.FC = () => {
  // Footer Resizer
  const footerResizer = useResizable({
    axis: "y",
    initial: 150,
    min: 50,
    reverse: true,
  })

  // Left Resizer
  const leftResizer = useResizable({
    axis: "x",
    initial: 250,
    min: 50,
  })

  // Right Resizer
  const rightResizer = useResizable({
    axis: "x",
    initial: 200,
    min: 50,
    reverse: true,
  })

  return (
    <div className="flex flex-column h-screen bg-dark font-mono color-white overflow-hidden w-100">
      <div className="flex grow">
        <div
          className={joinClassNames(
            "shrink-0 contents",
            leftResizer.isDragging && "dragging",
          )}
          style={{ width: leftResizer.position }}
        >
          Left Sidebar
        </div>
        <SampleSeparator
          isDragging={leftResizer.isDragging}
          {...leftResizer.separatorProps}
        />
        <div className="flex grow">
          <div className="grow bg-darker contents">Editor</div>
          <SampleSeparator
            isDragging={rightResizer.isDragging}
            {...rightResizer.separatorProps}
          />
          <div
            className={joinClassNames(
              "shrink-0 contents",
              rightResizer.isDragging && "dragging",
            )}
            style={{ width: rightResizer.position }}
          >
            Right Sidebar
          </div>
        </div>
      </div>
      <SampleSeparator
        dir="horizontal"
        isDragging={footerResizer.isDragging}
        {...footerResizer.separatorProps}
      />
      <div
        className={joinClassNames(
          "shrink-0 bg-darker contents",
          footerResizer.isDragging && "dragging",
        )}
        style={{ height: footerResizer.position }}
      >
        footer
      </div>
    </div>
  )
}

export default ResizeableLayoutDemo;
