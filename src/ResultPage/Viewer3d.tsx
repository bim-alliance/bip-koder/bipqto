import { useEffect, useRef, useState } from "react"
import { Button, Loader } from "@mantine/core"
import { useBimContext } from "../utils/useBimContext"
import { useAppSelector } from "../app/hooks"
import { IconPlus } from "@tabler/icons-react"

type ViewerProps = {
  bgColor?: string
  onClickOnItem?: (globalIds: Array<string>) => void
  performanceThreshold?: number
  showStat?: boolean
}

const Viewer3d: React.FC<ViewerProps> = ({ onClickOnItem, performanceThreshold = 0, bgColor = 'rgb(209,233,244)', showStat = false }) => {
  const containerRef = useRef<HTMLDivElement>(null);
  const inputFileRef = useRef<HTMLInputElement>(null);

  const globalId = useAppSelector((state) => state.bim.selectedGlobalId);
  const bimItems = useAppSelector((state) => state.bim.bimItems);
  const selectedItem = bimItems.find(item => item.globalId === globalId);

  const { initWorld, onHighlightItem, loadIfcFromFile } = useBimContext();
  const [isLoadingModel, setIsLoadingModel] = useState(false);

  useEffect(() => {
    if (selectedItem) {
      onHighlightItem(selectedItem.expressId);
    }
  }, [selectedItem, onHighlightItem])


  useEffect(() => {
    const onElementSelected = (expressIds: any) => {
      const globalIds = bimItems
        .filter((item) => expressIds.includes(item.expressId))
        .map((item) => item.globalId)

      onClickOnItem && onClickOnItem(globalIds)
    }
    const setup = async () => {
      await initWorld(containerRef, {
        bgColor,
        showStat,
        onClick: onElementSelected,
        performanceThreshold,
      });
    }
    setup();
  }, [initWorld, onClickOnItem, bimItems, showStat, performanceThreshold]);

  const onAddModel = () => {
    if (inputFileRef?.current) {
      inputFileRef.current.click();
    }
  }
  const onFileChange = async (e: any) => {
    if (e.target.files.length === 0) {
      return;
    }
    setIsLoadingModel(true);
    for (const file of e.target.files) {
      await loadIfcFromFile(file)
    }
    setIsLoadingModel(false)
  }

  return (
    <div style={{ border: '1px solid black', height: "99%", width: "100%"}}>
      <div style={{ position: 'absolute', left: '10px', right: '15px',bottom: "20px" }}>
        <input
          multiple
          type="file"
          style={{ display: 'none'}}
          ref={inputFileRef}
          onChange={onFileChange}
        />
        {isLoadingModel && <Loader size="sm" color="white" /> }
        {!isLoadingModel && <Button color="orange" onClick={onAddModel} >Lägg till fler filer
            <IconPlus style={{ marginLeft: '5px' }} />
        </Button> }
      </div>
      <div style={{ flex: 1, height: "100%", width: "100%" }} ref={containerRef} />
    </div>
  );
}

export default Viewer3d;

