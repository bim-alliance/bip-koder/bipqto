import { Paper, Tabs } from "@mantine/core"
import { useAppSelector } from "../app/hooks"
import { selectSingleBimItem } from "../bimSlice"
import { IBimItem } from "../types"
import { useEffect, useState } from "react"
import { AgGridReact } from "ag-grid-react"
import { ColDef } from "ag-grid-community"
import { IconCircleCheckFilled } from "@tabler/icons-react"

const DetailItem: React.FC = () => {
  const selectedBimItem = useAppSelector(selectSingleBimItem)

  return (
    <Paper
      style={{ height: "100%", width: "100%" }}
      withBorder
      shadow="md"
      className="bg-dark"
    >
      {selectedBimItem != null && <BimTabs bimItem={selectedBimItem} />}
    </Paper>
  )
}

const BimTabs: React.FC<{bimItem: IBimItem}> = ({bimItem}) => {
  const [tabs, setTabs] = useState<string[]>([]);

  const [activeTab, setActiveTab] = useState<string | null>('BIP');

  useEffect(() => {
    const psets = Object.keys(bimItem.psets ?? {})?.reduce((acc, pset) => {
      acc.push(pset);
      return acc;
    }, [] as string[]);
    const tabs = ["Allmänt", ...psets];
    if (bimItem.zodErrors != null) {
      tabs.push("Fel");
    }
    setTabs(tabs);

  }, [bimItem]);

  return (
    <Tabs value={activeTab} defaultValue="gallery" onChange={setActiveTab}>
      <Tabs.List>
        {tabs.map(tab => (
          <Tabs.Tab value={tab}>
            {tab === "Fel" && (
              <IconCircleCheckFilled style={{ color: "red" }} />
            )}
            {tab}
          </Tabs.Tab>
        ))}
      </Tabs.List>
      <>
        {bimItem.psets != null &&
          activeTab !== "Allmänt" &&
          activeTab !== "Fel" && (
            <LabelValueTable
              data={
                bimItem.psets[activeTab as string] as unknown as Record<
                  string,
                  string
                >
              }
            />
          )}
        {activeTab === "Allmänt" && <AllmantTable bimItem={bimItem} />}
        {activeTab === "Fel" && bimItem.zodErrors != null && <ErrorTab zodErrors={bimItem.zodErrors as unknown as any[]} />}
      </>
    </Tabs>
  )
}

export default DetailItem;

const ErrorTab:React.FC<{zodErrors: any[]}> = ({zodErrors}) => {
  let copyOfZodErrors = JSON.parse(JSON.stringify(zodErrors));
  delete copyOfZodErrors["_errors"];
  return <div>
    <pre>{JSON.stringify(copyOfZodErrors, null ,2)}</pre>
  </div>
}

const LabelValueTable: React.FC<{data: Record<string, string>}> = ({data}) => {

  const rows = Object.keys(data).map(key => {
    return {
      label: key,
      value: data[key]
    }
  });

  const columns: ColDef<{label: string, value: string}>[] = [
    {
      field: "label",
      headerName: "Egenskap",
      width: 200,
      sort: "asc"
    },
    {
      field: "value",
      headerName: "Värde",
      flex: 1
    }
  ]

  return <div style={{ width: "100%", height: "500px" }} className="ag-theme-balham">
    <AgGridReact rowData={rows} columnDefs={columns}/>
  </div>
}

const AllmantTable:React.FC<{bimItem: IBimItem}> = ({bimItem}) => {

  const columns:any = [
    {
      field: "label",
      headerName: "Egenskap",
      width: 200
    },
    {
      field: "value",
      headerName: "Värde",
      flex: 1
    }
  ];

  const rows = [
    {
      label: "Global Id",
      value: bimItem.globalId
    },
    {
      label: "Instansnamn",
      value: bimItem.name
    },
    {
      label: "Typnamn",
      value: bimItem.type?.name ?? ""
    },
    {
      label: "Instansklass",
      value: bimItem.instanceClass
    },
    {
      label: "Typklass",
      value: bimItem.type?.typeClass ?? ""
    }
  ]

  return <div style={{ width: "100%", height: "500px" }} className="ag-theme-balham">
    <AgGridReact rowData={rows} columnDefs={columns}/>
  </div>
}
