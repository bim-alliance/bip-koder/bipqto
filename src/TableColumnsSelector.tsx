import React from "react"
import { useAppDispatch, useAppSelector } from './app/hooks';
import { MultiSelect, Title } from '@mantine/core';
import { thunkToggleColumnVisibility } from './bimSlice';

const TableColumnsSelector:React.FC = () => {
  const dispatch = useAppDispatch();
  const tableColumns = useAppSelector((state) => state.bim.tableColumns);
  const activeTableColumns = tableColumns.filter((column) => !column.hide).map((column) => column.headerName);

  return <div>
    <Title size="h6">Välj kolumner</Title>
    <MultiSelect
      placeholder="Välj kolumner"
      data={tableColumns.map((col) => col.headerName as string)}
      value={activeTableColumns as string[]}
      searchable
      onChange={(cols) => dispatch(thunkToggleColumnVisibility(cols))}
      maxDropdownHeight={200}
    />
  </div>
};

export default TableColumnsSelector;
