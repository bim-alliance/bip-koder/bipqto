import type { IfcAPI } from 'web-ifc';
import type * as IfcBaseEntity from './webIfcTypes';
import type { IProperty } from './webIfcTypes';
import type { IIfcType, INumberArrayMap, IPropertyOrQuantitySet, IPsetOrQset, ITempSingleProperty } from './types';

export const getPropertySetsFromHashmap = (
  client: IfcAPI,
  expressID: number,
  relhashmap: INumberArrayMap | undefined
): IPropertyOrQuantitySet => {
  if (!relhashmap) return {};
  const relatedPsetsToGivenBimItem = relhashmap[expressID];
  if (!relatedPsetsToGivenBimItem) return {};

  return relatedPsetsToGivenBimItem.reduce<IPropertyOrQuantitySet>((acc, psetExpressId) => {
    const propertySetOrQuantites = client.GetLine(0, psetExpressId) as IfcBaseEntity.IRootObject;
    const instanceClass = propertySetOrQuantites.__proto__.constructor.name;

    if (instanceClass === 'IfcPropertySet') {
      const properties = getPropertiesForPset(client, propertySetOrQuantites);
      const name = propertySetOrQuantites.Name?.value;
      if (properties == null || name == null) return acc;
      const formattedPsetName = replaceAllDotsWithUnderline(name);
      acc[formattedPsetName] = properties.reduce<IPsetOrQset>((accProp, propertyItem) => {
        const formattedPropertyName = replaceAllDotsWithUnderline(propertyItem.property);
        accProp[formattedPropertyName] = propertyItem.value;
        return accProp;
      }, {});
    }
    return acc;
  }, {});
};

const replaceAllDotsWithUnderline = (str: string): string => {
  if (str == null) {
    return '';
  }
  return str.replace(/\./g, '_');
};

export const getPropertiesForPset = (client: IfcAPI, propertySet: IfcBaseEntity.IRootObject): IProperty[] => {
  const res = propertySet.HasProperties?.reduce<IProperty[]>((acc, cur) => {
    let ifcsingleProperty: ITempSingleProperty;
    try {
      ifcsingleProperty = client.GetLine(0, cur.value);
    } catch (error) {
      return acc; // Continue with next property
    }

    const simpleValue = ifcsingleProperty.NominalValue;
    if (!simpleValue || simpleValue.value == null) {
      return acc;
    }

    const item: IProperty = {
      property: ifcsingleProperty.Name?.value ?? '',
      value: simpleValue.value,
    };

    return acc.concat(item);
  }, []);

  return res ?? [];
};

type IItemKey =
  | 'RelatingPropertyDefinition'
  | 'RelatingType'
  | 'RelatingObject'
  | 'RelatingStructure'
  | 'RelatingMaterial'
  | 'RelatingPort';

type IObjectKey = 'RelatedObjects' | 'RelatedElements' | 'RelatedElement' | 'RelatedPort';

type IGetRelationsArgs = {
  client: IfcAPI;
  ifcConstant: number;
  itemKey: IItemKey;
  objectKey: IObjectKey;
};

export const getRelationsWithRelatingObjectHashmap = (args: IGetRelationsArgs): INumberArrayMap | undefined => {
  const lines = args.client.GetLineIDsWithType(0, args.ifcConstant);

  const nrOfRels = lines.size();
  const hashMap: INumberArrayMap = {};

  for (let i = 0; i < nrOfRels; i++) {
    const relItem: any = args.client.GetLine(0, lines.get(i));

    if (relItem == null) {
      continue;
    }

    //eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
    const relatedBimObjects: number[] = Array.isArray(relItem[args.objectKey])
      ? //eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
      relItem[args.objectKey].map((obj: { value: string }) => Number(obj.value))
      : [];

    const nrOfBimObjects = relatedBimObjects.length;
    for (let y = 0; y < nrOfBimObjects; y++) {
      const key = relatedBimObjects[y];
      //eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
      if (key != null && typeof hashMap[key] === 'undefined') {
        //eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call
        hashMap[key] = [relItem[args.itemKey].value];
        //eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-argument
      } else if (key != null) {
        //eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-argument
        hashMap[key].push(relItem[args.itemKey].value);
      }
    }
  }
  return hashMap;
};




export const getRelatingTypeFromHashmap = (
  client: IfcAPI,
  expressID: number,
  relhashmap: INumberArrayMap | undefined
): IIfcType | null => {
  if (!relhashmap) return null;
  const relatedTypesToGivenBimItem = relhashmap[expressID];
  if (!relatedTypesToGivenBimItem) return null;

  const relatedTypeExpressId = relatedTypesToGivenBimItem[0];

  let mcType: IIfcType | null = null;

  const relatingType: IfcBaseEntity.IRootObject = client.GetLine(0, relatedTypeExpressId);
  if (relatingType == null) {
    return null;
  }

  mcType = {
    globalId: relatingType.GlobalId.value,
    name: relatingType.Name?.value ?? '',
    typeClass: relatingType.__proto__.constructor.name,
    psets: {},
    expressId: relatingType.expressID,
  };

  if (!relatingType.HasPropertySets?.length) return mcType;

  const propertySets = relatingType.HasPropertySets.reduce<IPropertyOrQuantitySet>((acc, cur) => {
    if (cur.value == null) return acc;

    const propertySet = client.GetLine(0, cur.value) as IfcBaseEntity.IRootObject;
    const properties = getPropertiesForPset(client, propertySet);
    if (!properties.length) return acc;
    if (!propertySet.Name?.value) return acc;

    acc[propertySet.Name.value] = properties.reduce<IPsetOrQset>((accProp, propertyItem) => {
      accProp[propertyItem.property] = propertyItem.value;
      return accProp;
    }, {});

    return acc;
  }, {});

  return { ...mcType, psets: propertySets };
};
