import {
  Anchor,
  Avatar,
  Button,
  Center,
  Grid,
  Text,
  Title,
  useMantineTheme,
} from "@mantine/core"
import DropSection from "./DropSection/DropSection"
import VSpace from "../VSpace"

import { IconExternalLink, IconMail } from "@tabler/icons-react"

const StartPage = () => {
  const theme = useMantineTheme()

  const developers = [
    {
      name: "Per Ström",
      image:
        "https://media.licdn.com/dms/image/v2/C4D03AQG376K0v9_uYA/profile-displayphoto-shrink_800_800/profile-displayphoto-shrink_800_800/0/1565852725231?e=1736380800&v=beta&t=hOTfScCx81hrkfgOXziGokT88A6cazZz7xxbyG8DVKU",
      urlLinkedin: "https://www.linkedin.com/in/per-str%C3%B6m-1a56b810/",
    },
    {
      name: "Jonaatan Jacobsson",
      image:
        "https://media.licdn.com/dms/image/v2/C4E03AQH-52Az5pqtZg/profile-displayphoto-shrink_800_800/profile-displayphoto-shrink_800_800/0/1620720434500?e=1736985600&v=beta&t=xEFn7_kNQUWt-UYAcDuD5P5QqDE-5g2BOkAFP8Tu490",
      urlLinkedin: "https://www.linkedin.com/in/jonatanjacobsson",
    },
  ]

  return (
    <Grid style={{ overflow: "auto", height: "100%" }}>
      <Grid.Col span={2}></Grid.Col>
      <Grid.Col span={8}>
        <div
          style={{
            marginTop: theme.spacing.xl,
            position: "relative",
            border: "1px solid #ccc",
            backgroundColor: theme.colors.gray[0],
          }}
        >
          <DropSection />
        </div>
        <>
          <VSpace />
          <div
            style={{
              padding: theme.spacing.md,
              backgroundColor: theme.colors.blue[0],
            }}
          >
            <Title order={2}>Om BIP QTO</Title>
            <Text>
              BIP QTO är ett verktyg för att hantera och extrahera information
              från IFC-filer. Primära syftet är för entreprenörer att enkelt
              kunna få beräkning av mängder som ger underlag för kalkyl av
              kostnader, planering, inköp mm. Projektet finansierads delvis av{" "}
              <Anchor href="https://www.sbuf.se/" target="_blank">
                SBUF
              </Anchor>{" "}
              och{" "}
              <Anchor
                href="https://www.in.se/om-oss/paverka/sok-medel/etu/#/"
                target="_blank"
              >
                ETU
              </Anchor>{" "}
              under 2024.
            </Text>
          </div>
          <div
            style={{
              padding: theme.spacing.md,
              backgroundColor: theme.colors.blue[0],
              marginTop: theme.spacing.md,
            }}
          >
            <Title order={2}>Bidra till BIP QTO</Title>
            <Text>
              BIP QTO är såkallad "Open Source". Det innebär att vem som helst
              kan bidra till utvecklingen av verktyget. Målet är att skapa ett
              levande verktyg som utvecklas av användare för användare. På detta
              sätt kan vi hålla verktyget up-to-date gällande IFC-versioner,
              ändrade arbetssätt etc.
            </Text>
            <VSpace />
            <Title order={3}>Bidra med Förslag</Title>
            <Text>
              Har du förslag på en förbättring eller hittat en bugg? Maila in
              ett ärende på GitLab.
            </Text>
            <Button
              component="a"
              href="mailto:contact-project+bim-alliance-bip-koder-bipqto-58974046-issue-@incoming.gitlab.com"
              style={{ marginTop: theme.spacing.md, width: "200px" }}
            >
              Skicka mail
              <IconMail size={16} />
            </Button>
            <VSpace />
            <Title order={3}>Bidra med kod</Title>
            <Text>
              Är du intresserad av eller har erfarenhet av att skriva
              bim-applikationer? Då är du välkommen att bidra med att lösa
              buggar, lägga till funktioner, justera gränssnitt etc. Du hittar
              källkoden på GitLab.
            </Text>
            <Button
              component="a"
              href="https://gitlab.com/bim-alliance/bip-koder/bipqto"
              target="_blank"
              style={{ marginTop: theme.spacing.md, width: "200px" }}
            >
              Gå till källkod
              <IconExternalLink size={16} />
            </Button>
            <VSpace />
            <Title order={3}>Utvecklare som bidragit</Title>
            <div
              style={{
                padding: theme.spacing.md,
                marginTop: theme.spacing.md,
              }}
            >
              <Grid>
                {developers.map((developer, index) => (
                  <Grid.Col span={1} key={index}>
                    <Center>
                      <Avatar
                        src={developer.image}
                        alt={developer.name}
                        size="lg"
                      />
                    </Center>
                    <Center>
                      <Anchor href={developer.urlLinkedin} target="_blank">
                        <Text>{developer.name}</Text>
                      </Anchor>
                    </Center>
                  </Grid.Col>
                ))}
              </Grid>
            </div>
          </div>
        </>
      </Grid.Col>
    </Grid>
  )
}

export default StartPage
