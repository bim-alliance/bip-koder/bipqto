import { useEffect, useRef, useState } from "react"
import * as WebIfc from "web-ifc"
import { zodToJsonSchema } from "zod-to-json-schema"

import {
  Anchor,
  Button,
  Flex,
  Group,
  LoadingOverlay,
  Modal,
  Switch,
  Title,
  useMantineTheme,
} from "@mantine/core"
import FileUploader from "./FileUploader"
import { useDisclosure } from "@mantine/hooks"
import { useAppDispatch, useAppSelector } from "../../app/hooks"
import { useBimContext } from "../../utils/useBimContext"
import { EXTRACTION_CONFIGS } from "../../extractionConfigs"
import {
  rAddBimItems,
  rSetActiveExtractionConfig,
  rSetLoadingIfc,
  rSetPage,
  rSetProfileOrBipProperties,
  rSetRunValidationOnColumns,
  thunkUpdateTableColumnsOnExtractionConfigParams,
} from "../../bimSlice"
import { IExtractionConfig } from "../../types"
import { getBimItemsFromIfcFile } from "../../parseIfcFile"
import { LayoutComboBox } from "../../LayoutComboBox"
import TableColumnsSelector from "../../TableColumnsSelector"
import VSpace from "../../VSpace"

const DropSection: React.FC = () => {
  const dispatch = useAppDispatch()
  const hasLoaded = useRef(false)
  const { setBuffer } = useBimContext()

  const selectActiveExtractionConfig = useAppSelector(
    state => state.bim.activeExtractionConfig,
  )

  const selectColumns = useAppSelector(state => state.bim.tableColumns)

  const selectRunValidationOnColumns = useAppSelector(
    state => state.bim.runValidationOnColumns,
  )

  const loadingIfc = useAppSelector(state => state.bim.loadingIfc)

  const profileOrBipProperties = useAppSelector(
    state => state.bim.profileOrBipProperties,
  )

  const extractConfig = EXTRACTION_CONFIGS.find(
    config => config.name === selectActiveExtractionConfig,
  )

  const isInDev = import.meta.env.MODE === "development"
  useEffect(() => {
    if (!hasLoaded.current && isInDev) {
      //loadDemoFile();
      hasLoaded.current = true
    }
  }, [isInDev])

  async function loadDemoFile() {
    console.log("loading demo file")
    const url = isInDev ? "/src/examplefiles/example.ifc" : "example.ifc"
    const file = await fetch(url)
    console.log("file fetched")
    const fileName = file.url.split("/").pop()
    await parseAndDispatch(file, fileName ?? "")
    goToPage()
  }

  const filesUpload = async (file: File) => {
    await parseAndDispatch(file, file.name)
    goToPage()
  }

  const goToPage = async () => {
    await new Promise(resolve => setTimeout(resolve, 1))
    if (
      (profileOrBipProperties === "BIP_PROPERTIES" &&
        selectRunValidationOnColumns) ||
      profileOrBipProperties === "PROFILE"
    ) {
      dispatch(rSetPage("PRERESULT"))
    } else {
      dispatch(rSetPage("RESULT"))
    }
  }

  const parseAndDispatch = async (file: File | Response, fileName: string) => {
    dispatch(rSetLoadingIfc(true))
    console.time("parse Ifc")
    const ifcApi = new WebIfc.IfcAPI()
    await ifcApi.Init()
    const data = await file.arrayBuffer()
    const buffer = new Uint8Array(data)

    if (profileOrBipProperties === "PROFILE") {
      dispatch(
        thunkUpdateTableColumnsOnExtractionConfigParams(
          extractConfig as IExtractionConfig,
        ),
      )
    }

    const args = {
      ifcApi,
      buffer,
      fileName,
      profileOrBipProperties,
      extractionConfig: extractConfig as IExtractionConfig,
      attributesConfig: selectColumns
        .filter(col => col.hide !== true)
        .map(column => column.field as string),
    }

    const bimItems = await getBimItemsFromIfcFile(args)
    console.timeEnd("parse Ifc")
    ifcApi.CloseModel(0)

    dispatch(rAddBimItems(bimItems))
    setBuffer(buffer)
    dispatch(rSetLoadingIfc(false))
  }
  const theme = useMantineTheme()

  return (
    <>
      <LoadingOverlay
        visible={loadingIfc}
        zIndex={1000}
        overlayProps={{ radius: "sm", blur: 2 }}
      />
      <div style={{ padding: theme.spacing.sm }}>
        <Group>
          <Button
            variant={
              profileOrBipProperties === "PROFILE" ? "filled" : "outline"
            }
            onClick={() => dispatch(rSetProfileOrBipProperties("PROFILE"))}
          >
            Välj profil
          </Button>
          <Button
            variant={
              profileOrBipProperties === "PROFILE" ? "outline" : "filled"
            }
            onClick={() =>
              dispatch(rSetProfileOrBipProperties("BIP_PROPERTIES"))
            }
          >
            Välj BIP-egenskaper
          </Button>
        </Group>
      </div>

      <div
        style={{
          backgroundColor: "#e6ebf3",
          padding: "10px",
          marginTop: "10px",
        }}
      >
        {profileOrBipProperties === "PROFILE" && (
          <>
            <LayoutComboBox
              onChange={item =>
                dispatch(rSetActiveExtractionConfig(item?.name))
              }
              options={EXTRACTION_CONFIGS}
              displayProperty="name"
              currentValue={extractConfig}
            />
            {extractConfig != null && (
              <ExtranConfigModal extractionConfig={extractConfig} />
            )}
          </>
        )}
        {profileOrBipProperties === "BIP_PROPERTIES" && (
          <div>
            <TableColumnsSelector />
            <Title size="h6">Kör validering</Title>
            <Switch
              checked={selectRunValidationOnColumns}
              onChange={event =>
                dispatch(
                  rSetRunValidationOnColumns(event.currentTarget.checked),
                )
              }
            />
          </div>
        )}
      </div>
      <FileUploader onFileUpload={filesUpload} />
      <Flex justify="center">
        <Button variant="filled" color="orange" onClick={() => loadDemoFile()}>
          Kör demo-fil
        </Button>
      </Flex>
      <VSpace />
    </>
  )
}

export default DropSection;

const ExtranConfigModal:React.FC<{extractionConfig: IExtractionConfig}> = ({extractionConfig}) => {
  const [opened, { open, close }] = useDisclosure(false);

  const [json, setJson] = useState<any>(null);


  useEffect(() => {
    const copyOfConfig = JSON.parse(JSON.stringify(extractionConfig));
    copyOfConfig.requirements = zodToJsonSchema(extractionConfig.requirements);
    setJson(copyOfConfig);
  }, []);

  return <>
    <Modal size="lg" opened={opened} onClose={close} title={extractionConfig.name}>
      <pre>
        {JSON.stringify(json, null, 2)}
      </pre>
    </Modal>
    <Anchor href="#" onClick={open}>Visa inställningar</Anchor>
  </>
}
