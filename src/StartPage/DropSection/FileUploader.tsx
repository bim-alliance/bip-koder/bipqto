import React, { useCallback, useState } from "react"
import { useDropzone } from "react-dropzone"
import { Alert } from "@mantine/core"

const FileUploader: React.FC<{ onFileUpload: (file: File) => void }> = ({
  onFileUpload,
}) => {
  const [error, setError] = useState<string | null>(null)

  const onDrop = useCallback(
    (acceptedFiles: any[]) => {
      if (acceptedFiles.length === 0) {
        setError("Du kan endast ladda upp en fil åt gången.")
      } else {
        setError(null)
        onFileUpload(acceptedFiles[0]) // Endast den första filen skickas vidare
      }
    },
    [onFileUpload],
  )

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    maxFiles: 1, // Begränsar till en fil
    accept: {
      "application/octet-stream": [".ifc"], // Begränsar till .ifc-filer
    },
  })

  return (
    <div>
      {error && (
        <Alert title="Fel" color="red">
          {error}
        </Alert>
      )}
      <div {...getRootProps()} className="dropZone">
        <input {...getInputProps()} />
        <p className="mb-0" style={{ cursor: "pointer" }}>
          Dra och släpp en .ifc-fil här, eller klicka för att välja en fil
        </p>
      </div>
    </div>
  )
}

export default FileUploader;
