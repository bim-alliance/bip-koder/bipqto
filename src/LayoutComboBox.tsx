import { Combobox, Input, InputBase, ScrollArea, useCombobox } from '@mantine/core';

interface ILayoutComboBox<T> {
  onChange: (layout: T | null) => void;
  options: T[];
  displayProperty: Path<T>;
  currentValue: T | null;
}

export const LayoutComboBox = <T,>({
                                     options,
                                     onChange,
                                     displayProperty,
                                     currentValue,
                                   }: ILayoutComboBox<T>) => {
  const combobox = useCombobox({
    onDropdownClose: () => combobox.resetSelectedOption(),
  });

  const doSelect = (option: T) => {
    combobox.closeDropdown();
    onChange(option);
  };

  return (
    <Combobox store={combobox}>
      <Combobox.Target>
        <InputBase
          variant="filled"
          component="button"
          type="button"
          pointer
          rightSection={<Combobox.Chevron />}
          rightSectionPointerEvents="none"
          onClick={() => combobox.toggleDropdown()}
          className="w-52"
        >
          {currentValue != null && getNestedValue(currentValue, displayProperty) != null ? (
            String(getNestedValue(currentValue, displayProperty))
          ) : (
            <Input.Placeholder>Select layout</Input.Placeholder>
          )}
        </InputBase>
      </Combobox.Target>

      <Combobox.Dropdown>
        <Combobox.Options>
          <ScrollArea.Autosize type="scroll" mah={300}>
            {options.map((option, index) => {
              const display = getNestedValue(option, displayProperty);
              const strDisplay = String(display);
              const optionElement = (
                <Combobox.Option key={index} value={strDisplay} onClick={() => doSelect(option)}>
                  {strDisplay}
                </Combobox.Option>
              );

              /*if (index === 1) {
                return [<Divider key="divider" />, optionElement];
              }*/

              return optionElement;
            })}
          </ScrollArea.Autosize>
        </Combobox.Options>
      </Combobox.Dropdown>
    </Combobox>
  );
};

// typer

type StringOrNumKeys<TObj> = keyof TObj & (string | number);

export type NestedFieldPaths<TData = any, TValue = any, TDepth extends any[] = []> = {
  [TKey in StringOrNumKeys<TData>]: TData[TKey] extends Function | undefined
    ? never
    : TData[TKey] extends any[] | undefined
      ? (TData[TKey] extends TValue ? `${TKey}` : never) | `${TKey}.${number}`
      :
      | (TData[TKey] extends TValue ? `${TKey}` : never)
      | NestedPath<TData[TKey], `${TKey}`, TValue, [...TDepth, any]>;
}[StringOrNumKeys<TData>];

type NestedPath<
  TValue,
  Prefix extends string,
  TValueNestedChild,
  TDepth extends any[],
> = TValue extends object
  ? `${Prefix}.${TDepth["length"] extends 5 ? any : NestedFieldPaths<TValue, TValueNestedChild, TDepth>}`
  : never;

export type Path<TData = any, TValue = any> = TData extends any
  ? NestedFieldPaths<TData, TValue, []>
  : never;

export const getNestedValue = <T, TValue>(obj: T, path: Path<T, TValue>): TValue | null => {
  const parts = path.split(".");
  let result: any = obj;

  for (let i = 0; i < parts.length; i++) {
    if (result == null) {
      return null;
    }

    result = result[parts[i]];
  }

  return result;
};
