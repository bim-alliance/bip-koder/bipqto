import { z } from "zod"
import { IExtractionConfig } from "./types"
import { BIP_PROPERTIES, GLOBAL_ATTR } from "./constants"

const IFC_CLASSES = {
  IFCFLOWSEGMENT: "IFCFLOWSEGMENT",
  IFCFLOWFITTING: "IFCFLOWFITTING",
  IFCCOVERING: "IFCCOVERING",
  IFCBUILDINGELEMENTBYPROXY: "IFCBUILDINGELEMENTBYPROXY",
}

const BIP_ZOD = {
  TypeID: z.string().min(1, { message: "TypeID måste vara minst 1 tecken lång" }),
  BSABwr: z.string().min(1, { message: "BSABWr måste vara minst 1 tecken lång" }),
  BSABe: z.string().min(1, { message: "BSABe måste vara minst 1 tecken lång" }),
  ProductCode: z.string().min(1, { message: "ProductCode måste vara minst 1 tecken lång" }),
  ConnectionSize: z.string().min(1, { message: "Connection måste vara minst 1 tecken lång" }),
  Length: z.number().gte(0, { message: "Length måste vara större än 0" }),
  InsulationType: z.string().min(1, { message: "InsulationType måste vara minst 1 tecken lång" }),
  InsulationThickness: z.string().min(1, { message: "InsulationThickness måste vara minst 1 tecken lång" }),
  ProductSize: z.string().min(1, { message: "ProductSize måste vara minst 1 tecken lång" }),
  CenterElevation: z.number().gte(0, { message: "CenterElevation måste vara större än 0" }),
  InstanceDescription: z.string().min(1, { message: "InstanceDescription måste vara minst 1 tecken lång" }),
}

export const EXTRACTION_CONFIGS: IExtractionConfig[] = [
  {
    name: "VVS - Styckvaror",
    includedIfcClasses: [],
    excludedIfcClasses: [IFC_CLASSES.IFCFLOWSEGMENT,IFC_CLASSES.IFCFLOWFITTING, IFC_CLASSES.IFCCOVERING],
    requirements: z.object({
      psets: z.object({
        BIP: z.object({
          TypeID: BIP_ZOD.TypeID,
          ProductCode: BIP_ZOD.ProductCode,
        }),
      }),
    }),
    groupBy: {
      properties:  [GLOBAL_ATTR.instanceClass, BIP_PROPERTIES.TypeID, BIP_PROPERTIES.ProductCode],
      sumType: "COUNT"
    }
  },
  {
    name: "VVS - Styckvaror - Höjdindelat",
    includedIfcClasses: [],
    excludedIfcClasses: [IFC_CLASSES.IFCFLOWSEGMENT,IFC_CLASSES.IFCFLOWFITTING, IFC_CLASSES.IFCCOVERING],
    requirements: z.object({
      psets: z.object({
        BIP: z.object({
          TypeID: BIP_ZOD.TypeID,
          ProductCode: BIP_ZOD.ProductCode,
        }),
      }),
    }),
    groupBy: {
      properties:  [BIP_PROPERTIES.TypeID, BIP_PROPERTIES.ProductCode],
      sumType: "SUM",
      sumProperty: BIP_PROPERTIES.Length,
    }
  },
  {
    name: "VVS - Rör/Kanaler",
    includedIfcClasses: [IFC_CLASSES.IFCFLOWSEGMENT],
    excludedIfcClasses: [],
    requirements: z.object({
      psets: z.object({
        BIP: z.object({
          TypeID: BIP_ZOD.TypeID,
          CenterElevation: BIP_ZOD.CenterElevation,
          ConnectionSize: BIP_ZOD.ConnectionSize,
          Length: BIP_ZOD.Length,
        }),
      }),
    }),
    groupBy: {
      properties:  [BIP_PROPERTIES.TypeID, BIP_PROPERTIES.ProductCode, BIP_PROPERTIES.ConnectionSize],
      sumType: "SUM",
      sumProperty: BIP_PROPERTIES.Length,
    }
  },
  {
    name: "VVS - Rör/Kanaler - Höjdindelat",
    includedIfcClasses: [IFC_CLASSES.IFCFLOWSEGMENT],
    excludedIfcClasses: [],
    requirements: z.object({
      psets: z.object({
        BIP: z.object({
          CenterElevation: BIP_ZOD.CenterElevation,
          Length: BIP_ZOD.Length,
          ProductSize: BIP_ZOD.ProductSize,
        }),
      }),
    }),
    groupBy: {
      properties: [BIP_PROPERTIES.TypeID, BIP_PROPERTIES.ProductSize],
      sumType: "SUM",
      sumProperty: BIP_PROPERTIES.Length,
      heightProperty: BIP_PROPERTIES.CenterElevation,
      heightSumInterval: [[0,1799], [1800, 3000], [3001, 4500], [4501, 7000], [7000, null]]
    }
  },
  {
    name: "El - Elstegar",
    includedIfcClasses: [IFC_CLASSES.IFCFLOWSEGMENT],
    excludedIfcClasses: [],
    requirements: z.object({
      psets: z.object({
        BIP: z.object({
          TypeID: BIP_ZOD.TypeID,
          Length: BIP_ZOD.Length,
        }),
      }),
    }),
    groupBy: {
      properties: [BIP_PROPERTIES.TypeID],
      sumType: "SUM",
      sumProperty: BIP_PROPERTIES.Length,
      heightProperty: BIP_PROPERTIES.CenterElevation,
      heightSumInterval: [[0,1799], [1800, 3000], [3001, 4500], [4501, 7000], [7000, null]]
    }
  },
  {
    name: "El - Elstegar - Höjdindelat",
    includedIfcClasses: [IFC_CLASSES.IFCFLOWSEGMENT],
    excludedIfcClasses: [],
    requirements: z.object({
      psets: z.object({
        BIP: z.object({
          TypeID: BIP_ZOD.TypeID,
          Length: BIP_ZOD.Length,
          CenterElevation: BIP_ZOD.CenterElevation,
        }),
      }),
    }),
    groupBy: {
      properties:  [BIP_PROPERTIES.TypeID],
      sumType: "SUM",
      sumProperty: BIP_PROPERTIES.Length,
    }
  },
  {
    name: "El - Styckvaror",
    includedIfcClasses: [],
    excludedIfcClasses: [IFC_CLASSES.IFCFLOWSEGMENT,IFC_CLASSES.IFCFLOWFITTING, IFC_CLASSES.IFCCOVERING],
    requirements: z.object({
      psets: z.object({
        BIP: z.object({
          TypeID: BIP_ZOD.TypeID,
          ProductCode: BIP_ZOD.ProductCode,
        }),
      }),
    }),
    groupBy: {
      properties:  [GLOBAL_ATTR.instanceClass, BIP_PROPERTIES.TypeID, BIP_PROPERTIES.InstanceDescription],
      sumType: "COUNT"
    }
  }
];
