import React from "react"
import {
  Button,
  Card,
  Flex,
  Grid,
  Progress,
  Text,
  Tooltip,
  useMantineTheme,
} from "@mantine/core"
import { useAppDispatch, useAppSelector } from "../app/hooks"
import {
  rSetPage,
  selectErrorsGroupedByTypeClass,
  selectStats,
} from "../bimSlice"
import { AgGridReact } from "ag-grid-react"
import { CellStyle, ColDef, ICellRendererParams } from "ag-grid-community"
import "ag-grid-community/styles/ag-grid.css" // Ma

const PreResultPage: React.FC = () => {
  const dispatch = useAppDispatch()

  const theme = useMantineTheme()

  const typeClassValidationRows = useAppSelector(selectErrorsGroupedByTypeClass)

  const stats = useAppSelector(selectStats)

  const columns: ColDef[] = [
    {
      field: "ifcTypeClass",
      headerName: "IFC Typklass",
      cellStyle: params => {
        if (params.node.rowPinned) {
          return { fontWeight: "bold" } as CellStyle
        }
        return {}
      },
    },
    { field: "nrOfObjects", headerName: "Antal objekt", hide: true },
    {
      field: "nrOfValidObjects",
      headerName: "Antal giltiga objekt",
      hide: true,
    },
    {
      field: "nrOfInvalidObjects",
      headerName: "Antal ogiltiga objekt",
      hide: true,
    },
    {
      field: "Validering",
      headerName: "Validering",
      flex: 1,
      cellRenderer: (params: ICellRendererParams) => {
        const totalNrOfObjects = params.data.nrOfObjects
        const totalNrOfValidObjects = params.data.nrOfValidObjects
        const totalNrOfInvalidObjects = params.data.nrOfInvalidObjects

        const correctObjectStr = `Korrekta objekt: ${totalNrOfValidObjects}`
        const incorrectObjectStr = `Felaktiga objekt: ${totalNrOfInvalidObjects}`

        const percentCorrect = Math.round(
          (totalNrOfValidObjects / totalNrOfObjects) * 100,
        )
        const percentIncorrect = Math.round(
          (totalNrOfInvalidObjects / totalNrOfObjects) * 100,
        )
        return (
          <Progress.Root size="xl">
            <Tooltip label={correctObjectStr}>
              <Progress.Section value={percentCorrect} color="green">
                <Progress.Label>{percentCorrect} %</Progress.Label>
              </Progress.Section>
            </Tooltip>

            <Tooltip label={incorrectObjectStr}>
              <Progress.Section
                value={(totalNrOfInvalidObjects / totalNrOfObjects) * 100}
                color="red"
              >
                <Progress.Label>{percentIncorrect} %</Progress.Label>
              </Progress.Section>
            </Tooltip>
          </Progress.Root>
        )
      },
    },
  ]

  const summaryRow = {
    ifcTypeClass: "Total",
    nrOfObjects: stats.countAfterFilter,
    nrOfValidObjects: stats.valid.count,
    nrOfInvalidObjects: stats.invalid.count,
  }

  return (
    <Grid style={{ height: "500px" }}>
      <Grid.Col span={3} />
      <Grid.Col
        span={6}
        style={{
          marginTop: theme.spacing.xl,
          position: "relative",
          border: "1px solid #ccc",
          backgroundColor: theme.colors.gray[0],
          height: "100%",
        }}
      >
        <Flex direction="column" justify="center" align="center">
          <h3>Resultat av validering</h3>
          <ResultCards />
          <div
            style={{ height: "500px", width: "100%", marginTop: "20px" }}
            className="ag-theme-balham" // applying the Data Grid theme
          >
            <AgGridReact
              rowData={typeClassValidationRows}
              columnDefs={columns}
              rowHeight={40}
              defaultColDef={{
                cellStyle: { fontSize: "14px" },
              }}
              pinnedTopRowData={[summaryRow]}
            />
          </div>
          <Button
            size="lg"
            style={{ marginTop: "20px" }}
            onClick={() => dispatch(rSetPage("RESULT"))}
          >
            Gå til resultat
          </Button>
        </Flex>
      </Grid.Col>
      <Grid.Col span={3} />
    </Grid>
  )
}

export default PreResultPage;

const ResultCards:React.FC = () => {
  const stats = useAppSelector(selectStats);

  return <Grid>
    <Grid.Col span={3}>
      <Card shadow="sm" padding="lg" style={{ backgroundColor: 'gray', color: 'white' }}>
        <Text style={{ fontWeight: 500 }}>Totalt</Text>
        <Text>{stats.total}st</Text>
      </Card>
    </Grid.Col>
    <Grid.Col span={3}>
      <Card shadow="sm" padding="lg" style={{ backgroundColor: 'gray', color: 'white' }}>
        <Text style={{ fontWeight: 500 }}>I mall</Text>
        <Text>{stats.countAfterFilter}st</Text>
      </Card>
    </Grid.Col>
    <Grid.Col span={3}>
      <Card shadow="sm" padding="lg" style={{ backgroundColor: 'green', color: 'white' }}>
        <Text style={{ fontWeight: 500 }}>Korrekta</Text>
        <Text>{stats.valid.percentage} %</Text>
      </Card>
    </Grid.Col>
    <Grid.Col span={3}>
      <Card shadow="sm" padding="lg" style={{ backgroundColor: 'red', color: 'white' }}>
        <Text style={{ fontWeight: 500 }}>Inkorrekta</Text>
        <Text>{stats.invalid.percentage} %</Text>
      </Card>
    </Grid.Col>
  </Grid>
};
