import { z, ZodFormattedError } from 'zod';

export type IPropertyOrQuantitySet = Record<string, IPsetOrQset>;
export type IPsetOrQset = Record<string, string | number>;

export interface IBimItem {
  fileName: string;
  expressId: number;
  globalId: string;
  instanceClass: string;
  name: string;
  psets: IPropertyOrQuantitySet | null;
  type: Omit<IIfcType, 'psets'> | null;
  zodErrors: ZodFormattedError<IBimItem> | null; // Används om användaren väljer profil
  missingAttributes: string[] | null; // Används om användaren väljer egna kolumner
  sum?:number;
}

export interface IGroupedItem {
  groupKey: string;
  count: number;
  heightInterval: string;
  sumValue: number;
  groupedProperties: Record<string, string | number>;
}

export interface INumberArrayMap {
  [key: number]: number[];
}


export type ITempSingleProperty = {
  Name?: { value?: string };
  NominalValue?: { value?: string | number };
};

export type IIfcType = {
  globalId: string;
  name: string;
  typeClass: string;
  psets?: IPropertyOrQuantitySet;
  expressId: number;
};


export type HeightSumIntervalType = ([number, number] | [number, null])[];

export interface IExtractionConfig {
  name: string;
  includedIfcClasses: string[];
  excludedIfcClasses: string[];
  requirements: z.ZodObject<any, any>;
  groupBy: {
    properties: string[],
    sumType: "COUNT" | "SUM",
    sumProperty?: string; // t.ex psets.BIP.Length, används om groupType är SUM
    heightProperty?: string; // t.ex psets.BIP.CenterElevation, used if groupType is SUM
    heightSumInterval?: HeightSumIntervalType;
  }
}

export interface ITableCol {
  headerName: string;
  field: string;
  hide: boolean;
}
