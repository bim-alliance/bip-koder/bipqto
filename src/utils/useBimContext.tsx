import type { SimpleWorld, MeshCullerRenderer } from '@thatopen/components';
import type { FragmentsGroup } from '@thatopen/fragments';
import Stats from "stats.js";
import type { RefObject } from 'react';

import {
  Components,
  Cullers,
  FragmentsManager,
  IfcLoader,
  SimpleCamera,
  SimpleScene,
  Worlds,
} from '@thatopen/components';
import { Highlighter, Outliner, PostproductionRenderer }  from "@thatopen/components-front";
import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useMemo,
  useState,
} from 'react';
import { MeshBasicMaterial, Color } from 'three';

type Url = string;
type WorldType = SimpleWorld<SimpleScene, SimpleCamera, PostproductionRenderer>;

type BimProviderProps = {
  children: React.ReactNode;
};

type WorldOptions = {
  bgColor: string,
  onClick?: (expressId: any) => void
  highlightColor?: string;
  showStat?: boolean
  performanceThreshold?: number
}

type BimContextProps = {
  components?: Components;
  world?: WorldType;
  model?: FragmentsGroup;
  initWorld: (containerRef: RefObject<HTMLDivElement>, options: WorldOptions) => void;
  loadIfcFromUrl: (url: Url) => Promise<void>;
  loadIfcFromFile: (file: File) => Promise<void>;
  setBuffer: (buffer: Uint8Array) => void;
  onHighlightItem: (expressId: number) => void;
};
type CreateMaterialProps = {
  components: Components;
  world?: WorldType;
  highlighter: Highlighter;
  outliner: Outliner;
}

const BimContext = createContext<BimContextProps>({} as BimContextProps);

export const useBimContext = () => useContext(BimContext);

let onClickHandler: any;
const SELECTED_COLOR = "SELECTED_COLOR";

const createBasicHoverMaterial = ({ outliner, components, world, highlighter }: CreateMaterialProps) => {
  outliner.world = world;
  outliner.enabled = true;

  outliner.create(
    "basic-hover",
    new MeshBasicMaterial({
      color: 0xbcf124,
      transparent: true,
      opacity: 0.5,
    }),
  );

  highlighter.events.select.onHighlight.add((fragmentIdMap: any) => {
    const keys = Object.keys(fragmentIdMap)
    const expressIds = keys.map((key) => {
      return [...fragmentIdMap[key]]
    })
    onClickHandler && onClickHandler(expressIds.flat());
    outliner.clear("basic-hover");
    outliner.add("basic-hover", fragmentIdMap);
  });

  highlighter.events.select.onClear.add(() => {
    if (outliner) {
      outliner.clear("basic-hover");
    }
  });
}
let useAsAMainModel = true;
export const BimProvider = ({ children }: BimProviderProps) => {
  const components = useMemo(() => new Components(), []);

  const cullers = components.get(Cullers);
  const fragmentsManager = components.get(FragmentsManager);
  const highlighter = components.get(Highlighter);
  const outliner = components.get(Outliner);
  const ifcLoader = components.get(IfcLoader);
  const intervalRef = useRef<number>();

  const [world, setWorld] = useState<WorldType>();
  const [model, setModel] = useState<FragmentsGroup>();
  const [preloadedBuffer, setBuffer] = useState<Uint8Array>();
  

  fragmentsManager.onFragmentsLoaded.add(async (loadedModel) => {
    if (useAsAMainModel) {
      setModel(loadedModel);
    }
  });

  const loadIfcFromUrl = useCallback(
    async (url: Url) => {
      if (!ifcLoader || !world) return;
      try {
        const file = await fetch(url);
        const data = await file.arrayBuffer();
        const buffer = new Uint8Array(data);
        await ifcLoader.load(buffer, false);
      } catch (error) {
        console.error(error);
        return;
      }
    },
    [ifcLoader, world],
  );

  const loadIfcFromFile = useCallback(
    async (file: File, setMainModel: boolean = false) => {
      if (!ifcLoader || !world) return;
      try {
        const data = await file.arrayBuffer();
        const buffer = new Uint8Array(data);
        useAsAMainModel = false
        await ifcLoader.load(buffer, false);
      } catch (error) {
        console.error(error);
        return;
      }
    },
    [ifcLoader, world],
  );

  const initWorld = useCallback(
    async (containerRef: RefObject<HTMLDivElement>, options: WorldOptions) => {
      if (world || !containerRef.current) return;
      const {
        performanceThreshold = 0,
        showStat = false,
        highlightColor = 'rgb(255, 0, 0)',
      } = options;

      const useCuller = performanceThreshold > 0;

      onClickHandler = options?.onClick;
      const worlds = components.get(Worlds);
      const simpleWorld = worlds.create<SimpleScene, SimpleCamera, PostproductionRenderer>();

      simpleWorld.scene = new SimpleScene(components);
      simpleWorld.renderer = new PostproductionRenderer(components, containerRef.current);
      simpleWorld.camera = new SimpleCamera(components);
      setWorld(simpleWorld);
      components.init();

      // Setup culler if needed
      let culler:MeshCullerRenderer;
      if (useCuller) {
        culler = cullers.create(simpleWorld);
        culler.threshold = performanceThreshold;
        culler.needsUpdate = true;
        simpleWorld.camera.controls.addEventListener("controlend", () => {
          culler.needsUpdate = true;
        });
        intervalRef.current = setInterval(() => {
          culler.needsUpdate = true;
        }, 200) 
      }

      simpleWorld.renderer.postproduction.enabled = true;
      simpleWorld.scene.setup();
      simpleWorld.renderer?.resize();
      simpleWorld.scene.config.backgroundColor = new Color(options?.bgColor || 'rgb(125, 5, 5)');
      simpleWorld.camera.updateAspect();
      
      ifcLoader.settings.webIfc.COORDINATE_TO_ORIGIN = true;
      await ifcLoader.setup();
      highlighter.setup({ world: simpleWorld });

      const color = new Color(highlightColor);
      highlighter?.add(SELECTED_COLOR, color);
      createBasicHoverMaterial({ outliner, components, world: simpleWorld, highlighter });

      fragmentsManager.onFragmentsLoaded.add(async (loadedModel) => {
        if (useCuller) {
          for (const fragment of loadedModel.items) {
              culler.add(fragment.mesh);
          }
          culler.needsUpdate = true;
        }
        simpleWorld.scene.three.add(loadedModel);
      });

      if (showStat) {
        renderStat(simpleWorld);
      }
      if (preloadedBuffer) {
        await ifcLoader.load(preloadedBuffer, false);
      }
    },
    [world, components, cullers, ifcLoader, fragmentsManager.onFragmentsLoaded, preloadedBuffer, highlighter, outliner],
  );

  /*
   * Show stat panel for debug and performance visibility
   */
  const renderStat = (simpleWorld: WorldType) => {
    const stats = new Stats();
    stats.showPanel(0);
    document.body.append(stats.dom);
    stats.dom.style.left = "0px";
    stats.dom.style.zIndex = "unset";
    simpleWorld.renderer?.onBeforeUpdate.add(() => stats.begin());
    simpleWorld.renderer?.onAfterUpdate.add(() => stats.end());
  }

  const onHighlightItem = (expressId: number) => {
    if (!model) {
      return;
    }
    const item = model.getFragmentMap([expressId]);
    highlighter.highlightByID(SELECTED_COLOR, item, true, true);
  }

  useEffect(() => {
    return () => {
      cullers?.dispose();
      components?.dispose();
      if (intervalRef.current) {
        clearInterval(intervalRef.current);
      }
    };
  }, [components, cullers]);

  return (
    <BimContext.Provider
      value={{
        initWorld,
        loadIfcFromFile,
        loadIfcFromUrl,
        setBuffer,
        onHighlightItem,
      }}
    >
      {children}
    </BimContext.Provider>
  );
};
