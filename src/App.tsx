import { useAppSelector } from "./app/hooks"
import ResultPage from "./ResultPage/ResultPage"
import { createTheme, MantineProvider } from "@mantine/core"
import { BimProvider } from "./utils/useBimContext"
import PreResultPage from "./PreResultPage/PreResultPage"
import StartPage from "./StartPage/StartPage"

const App = () => {
  const selectPage = useAppSelector(state => state.bim.page)
  const theme = createTheme({
    primaryColor: "blue",
    fontFamily: "Arial, sans-serif",
  })
  return (
    <>
      <MantineProvider theme={theme}>
        <BimProvider>
          {selectPage === "START" && <StartPage />}
          {selectPage === "RESULT" && <ResultPage />}
          {selectPage === "PRERESULT" && <PreResultPage />}
        </BimProvider>
      </MantineProvider>
    </>
  )
}

export default App
