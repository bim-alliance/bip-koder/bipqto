import type { PayloadAction } from "@reduxjs/toolkit"
import { createSelector } from "@reduxjs/toolkit"
import type { IBimItem, IExtractionConfig } from "./types"
import type { AppThunk, RootState } from "./app/store"
import type { ColDef } from "ag-grid-community"
import { createAppSlice } from "./app/createAppSlice"
import { TABLE_COLUMNS } from "./constants"
import {
  bimItemIsIncludedInFilter,
  getGroupdAgGridColumnDefs,
  getPrpertiesFromZodObject,
  getZodErrorsInBimItem,
  groupBimItems,
} from "./utils"
import { FragmentsGroup } from "@thatopen/fragments"
import { EXTRACTION_CONFIGS } from "./extractionConfigs"

export type IPage = "START" | "RESULT" | "PRERESULT"

export interface IBimSliceState {
  page: IPage;
  profileOrBipProperties: "PROFILE" | "BIP_PROPERTIES";
  bimItems: IBimItem[];
  selectedGlobalId: string | undefined;
  activeExtractionConfig: string | undefined | null;
  tableColumns: ColDef[];
  viewerModel?: FragmentsGroup;
  loadingIfc: boolean;
  runValidationOnColumns: boolean;
  showValid: boolean;
  showInvalid: boolean;
  tableMode: "GROUPED" | "INSTANCE";
}

const initialState: IBimSliceState = {
  bimItems: [],
  profileOrBipProperties: "PROFILE",
  page: "START",
  selectedGlobalId: "2$oRkgEIbAIv1WgG2NCnxx",
  activeExtractionConfig: EXTRACTION_CONFIGS[0].name,
  tableColumns: TABLE_COLUMNS,
  viewerModel: undefined,
  loadingIfc: false,
  runValidationOnColumns: true,
  showValid: true,
  showInvalid: true,
  tableMode: "INSTANCE",
}

export const thunkUpdateTableColumnsOnExtractionConfigParams = (config: IExtractionConfig):AppThunk => {
  return (dispatch, getState) => {
    const tablecolumns = getState().bim.tableColumns;
    const requiredProperties = getPrpertiesFromZodObject(config.requirements);
    const updatedTableColunmns = [...tablecolumns].map((column) => {
      return {
        ...column,
        hide: !requiredProperties.includes(column.field as string),
      };
    });
    dispatch(rSetTableColumns(updatedTableColunmns));
  };
};

export const thunkToggleColumnVisibility = (columns: string[]): AppThunk => {
  return (dispatch, getState) => {
    const selectTableColumns = getState().bim.tableColumns;
    const _tableColumns = [...selectTableColumns].map((column) => {
      return {
        ...column,
        hide: !columns.includes(column.headerName as string),
      };
    });
    dispatch(rSetTableColumns(_tableColumns));
  }
};


export const bimSlice = createAppSlice({
  name: "bim",
  initialState,
  reducers: ({
    rAddBimItems: (state, action: PayloadAction<any>) => {
      state.bimItems = action.payload
    },
    rSetPage: (state, action: PayloadAction<IPage>) => {
      state.page = action.payload
    },
    rSetSelectedGlobalId: (state, action: PayloadAction<string | undefined>) => {
      state.selectedGlobalId = action.payload
    },
    rSetActiveExtractionConfig: (state, action: PayloadAction<string | undefined | null>) => {
      state.activeExtractionConfig = action.payload
    },
    rSetTableColumns: (state, action: PayloadAction<ColDef[]>) => {
      (state.tableColumns as ColDef[]) = action.payload
    },
    rSetLoadingIfc: (state, action: PayloadAction<boolean>) => {
      state.loadingIfc = action.payload
    },
    rSetProfileOrBipProperties: (state, action: PayloadAction<"PROFILE" | "BIP_PROPERTIES">) => {
      state.profileOrBipProperties = action.payload
    },
    rSetRunValidationOnColumns: (state, action: PayloadAction<boolean>) => {
      state.runValidationOnColumns = action.payload
    },
    rSetShowValid: (state, action: PayloadAction<boolean>) => {
      state.showValid = action.payload
    },
    rSetShowInvalid: (state, action: PayloadAction<boolean>) => {
      state.showInvalid = action.payload
    },
    rSetTableMode: (state, action: PayloadAction<"GROUPED" | "INSTANCE">) => {
      state.tableMode = action.payload
    },
  }),
});

const selectAllBimItems = (state: RootState) => state.bim.bimItems;
const selectActiveExtractionConfig = (state: RootState) => state.bim.activeExtractionConfig;
const selectProfileOrBipProperties = (state: RootState) => state.bim.profileOrBipProperties;

export const selectBimItemsFromExtractionFilters = createSelector([selectActiveExtractionConfig, selectAllBimItems, selectProfileOrBipProperties],(activeExtractionConfig, bimItems, profileOrBipProperties):IBimItem[] => {
  if (bimItems.length === 0) {
    return [];
  }
  const extractionConfig = EXTRACTION_CONFIGS.find((config) => config.name === activeExtractionConfig) as IExtractionConfig;
  return bimItems.reduce((acc, bimItem) => {
    if (profileOrBipProperties === "BIP_PROPERTIES") {
      acc.push(bimItem);
      return acc;
    }
    if (bimItemIsIncludedInFilter(bimItem, extractionConfig)) {
      const zodErrors = getZodErrorsInBimItem(bimItem, extractionConfig.requirements);
      const newItem = { ...bimItem, zodErrors }; // Create a new object with zodErrors
      acc.push(newItem);
    }
    return acc;
  }, [] as IBimItem[]);
});

export const selectInvalidBimItems =  createSelector([selectBimItemsFromExtractionFilters], (bimItems) => {
  return bimItems?.filter((item) => item.zodErrors != null || item.missingAttributes != null);
});

export const selectValidBimItems = createSelector([selectBimItemsFromExtractionFilters], (bimItems) => {
  return bimItems?.filter((item) => item.zodErrors == null && item.missingAttributes == null);
});

export const selectGroupedBimItems = createSelector([selectBimItemsFromExtractionFilters, (state: RootState) => state.bim.activeExtractionConfig], (bimItems, activeExtractionConfig) => {
  const extractionConfig = EXTRACTION_CONFIGS.find((config) => config.name === activeExtractionConfig) as IExtractionConfig;
  return groupBimItems(bimItems, extractionConfig);
});

export const selectAgGridGroupColumns = createSelector([(state: RootState) => state.bim.activeExtractionConfig], (activeExtractionConfig):ColDef[] => {
  const extractionConfig = EXTRACTION_CONFIGS.find((config) => config.name === activeExtractionConfig) as IExtractionConfig;
  return getGroupdAgGridColumnDefs(extractionConfig);
});

export const selectRowData = createSelector([(state: RootState) => state.bim.tableMode, (state:RootState) => state.bim.showValid, (state:RootState) => state.bim.showInvalid, selectValidBimItems, selectInvalidBimItems, selectGroupedBimItems], (tableMode, showValid, showInvalid, validBimItems, invalidBimItems, groupedBimItems) => {
  if (tableMode === "GROUPED") {
    return groupedBimItems;
  }
  if (showValid && showInvalid) {
    return [...validBimItems, ...invalidBimItems];
  }
  if (showValid) {
    return validBimItems;
  }
  if (showInvalid) {
    return invalidBimItems;
  }
  return [];
});

export const selectStats = createSelector([selectValidBimItems, selectInvalidBimItems, selectBimItemsFromExtractionFilters, selectAllBimItems], (validBimItems, invalidBimItems, bimItemsAfterFilter, allBimItems) => {
  return {
    valid: {
      count: validBimItems.length,
      percentage: Math.round(validBimItems.length / bimItemsAfterFilter.length * 100)
    },
    invalid: {
      count: invalidBimItems.length,
      percentage: Math.round(invalidBimItems.length / bimItemsAfterFilter.length * 100)
    },
    countAfterFilter: bimItemsAfterFilter.length,
    total: allBimItems.length
  }
});

export const selectSingleBimItem = createSelector([selectAllBimItems, (state: RootState) => state.bim.selectedGlobalId], (bimItems, selectedGlobalId) => {
  return bimItems.find((item) => item.globalId === selectedGlobalId);
});

interface IIfcTypeClassErrors {
  ifcTypeClass: string; // "IfcProtectiveDevice", "IfcAirTerminal"
  nrOfObjects: number;
  nrOfValidObjects: number;
  nrOfInvalidObjects: number;
}

export const selectErrorsGroupedByTypeClass = createSelector([selectBimItemsFromExtractionFilters], (bimItems) => {
  return bimItems.reduce((acc: IIfcTypeClassErrors[], bimItem: IBimItem) => {
    const ifcTypeClass = bimItem.type?.typeClass;
    if (!ifcTypeClass) return acc;
    const existing = acc.find((item) => item.ifcTypeClass === ifcTypeClass);
    if (existing) {
      if (bimItem.zodErrors || bimItem.missingAttributes) {
        existing.nrOfInvalidObjects += 1;
      } else {
        existing.nrOfValidObjects += 1;
      }
      existing.nrOfObjects += 1;
    } else {
      acc.push({
        ifcTypeClass,
        nrOfObjects: 1,
        nrOfValidObjects: bimItem.zodErrors ? 0 : 1,
        nrOfInvalidObjects: bimItem.zodErrors ? 1 : 0,
      });
    }
    return acc;
  }, [] as IIfcTypeClassErrors[]);
});

export const {
  rSetTableColumns,
  rSetActiveExtractionConfig,
  rAddBimItems,
  rSetPage,
  rSetSelectedGlobalId,
  rSetLoadingIfc,
  rSetProfileOrBipProperties,
  rSetRunValidationOnColumns,
  rSetShowInvalid,
  rSetShowValid,
  rSetTableMode,
} = bimSlice.actions

