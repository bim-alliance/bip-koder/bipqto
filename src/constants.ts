import { ColDef } from "ag-grid-community"

export const EXCLUDED_IFC_CLASSES = [
  "IfcOpeningElement",
  "IfcBuilding",
  "IfcSite",
  "IfcBuildingStorey",
  "IfcDistributionPort",
]

export const BIP_PROPERTIES = {
  AcousticRating: "psets.BIP.AcousticRating",
  AFF: "psets.BIP.AFF",
   Angle: "psets.BIP.Angle",
  Area: "psets.BIP.Area",
  ArticleNumber: "psets.BIP.ArticleNumber",
  AverageClearHeight: "psets.BIP.AverageClearHeight",
  AverageGrossHeight: "psets.BIP.AverageGrossHeight",
  AverageHeight: "psets.BIP.AverageHeight",
  BottomElevation: "psets.BIP.BottomElevation",
  BottomElevationAbs: "psets.BIP.BottomElevationAbs",
  BPD: "psets.BIP.BPD",
  BPDStatus: "psets.BIP.BPDStatus",
  BSABe: "psets.BIP.BSABe",
  BSABs: "psets.BIP.BSABs",
  BSABwr: "psets.BIP.BSABwr",
  BuildingFMGUID: "psets.BIP.BuildingFMGUID",
  BuildingStoreyFMGUID: "psets.BIP.BuildingStoreyFMGUID",
  CableNumber: "psets.BIP.CableNumber",
  CenterElevation: "psets.BIP.CenterElevation",
  CenterElevationAbs: "psets.BIP.CenterElevationAbs",
  Colour: "psets.BIP.Colour",
  Comment: "psets.BIP.Comment",
  ConnectionSize: "psets.BIP.ConnectionSize",
  ContractID: "psets.BIP.ContractID",
  ["CoolingPower_W"]: "psets.BIP.CoolingPower_W",
  Current_A: "psets.BIP.Current_A",
  DamperPosition: "psets.BIP.DamperPosition",
  DateDesign: "psets.BIP.DateDesign",
  Description: "psets.BIP.Description",
  Diameter: "psets.BIP.Diameter",
  ElementID: "psets.BIP.ElementID",
  EndElevation: "psets.BIP.EndElevation",
  Fi2: "psets.BIP.Fi2",
  FireCompartment : "psets.BIP.FireCompartment ",
  FireRating: "psets.BIP.FireRating",
  FireSeparating: "psets.BIP.FireSeparating",
  FloorType: "psets.BIP.FloorType",
  ["Flow_ft3/s"]: "psets.BIP.Flow_ft3/s",
["Flow_l/min"]: "psets.BIP.Flow_l/min",
["Flow_l/s"]: "psets.BIP.Flow_l/s",
["Flow_m3/h"]: "psets.BIP.Flow_m3/h",
  FluidCode: "psets.BIP.FluidCode",
  FluidTemperature: "psets.BIP.FluidTemperature",
  FluidType: "psets.BIP.FluidType",
  FMGUID: "psets.BIP.FMGUID",
["Friction_Pa/m"]: "psets.BIP.Friction_Pa/m",
  GrossFloorArea: "psets.BIP.GrossFloorArea",
  HeatingPower_W: "psets.BIP.HeatingPower_W",
  Height: "psets.BIP.Height",
  Hyperlink: "psets.BIP.Hyperlink",
  IfcGUID: "psets.BIP.IfcGUID",
  InnerDiameter: "psets.BIP.InnerDiameter",
  InsertionPoint: "psets.BIP.InsertionPoint",
  InstanceDescription: "psets.BIP.InstanceDescription",
  InstanceMounting: "psets.BIP.InstanceMounting",
  InsulationCovering : "psets.BIP.InsulationCovering ",
  InsulationInternalMaterial: "psets.BIP.InsulationInternalMaterial",
  InsulationInternalThickness: "psets.BIP.InsulationInternalThickness",
  InsulationInternalType: "psets.BIP.InsulationInternalType",
  InsulationMaterial: "psets.BIP.InsulationMaterial",
  InsulationThickness: "psets.BIP.InsulationThickness",
  InsulationType: "psets.BIP.InsulationType",
  InvertElevationAbs: "psets.BIP.InvertElevationAbs",
  IPClass: "psets.BIP.IPClass",
  kvValue: "psets.BIP.kvValue",
  Layer: "psets.BIP.Layer",
  LayerVariable: "psets.BIP.LayerVariable",
  Length: "psets.BIP.Length",
  Littera: "psets.BIP.Littera",
  Locked: "psets.BIP.Locked",
  Manufacturer: "psets.BIP.Manufacturer",
  ManufacturerProcured: "psets.BIP.ManufacturerProcured",
  Material: "psets.BIP.Material",
  MountingLocation: "psets.BIP.MountingLocation",
  Name: "psets.BIP.Name",
  NetFloorArea: "psets.BIP.NetFloorArea",
  Number: "psets.BIP.Number",
  NumberOfPoles: "psets.BIP.NumberOfPoles",
  nValue: "psets.BIP.nValue",
  ObjectGroupID: "psets.BIP.ObjectGroupID",
  ObjectID: "psets.BIP.ObjectID",
  Occupancy: "psets.BIP.Occupancy",
  OccupancyNumber: "psets.BIP.OccupancyNumber",
  OccupancyType: "psets.BIP.OccupancyType",
  OuterDiameter: "psets.BIP.OuterDiameter",
  OverallHeight: "psets.BIP.OverallHeight",
  OverallWidth: "psets.BIP.OverallWidth",
  PipeSeries: "psets.BIP.PipeSeries",
  PowerElectrical_W: "psets.BIP.PowerElectrical_W",
  PowerTot_W: "psets.BIP.PowerTot_W",
  PressureDrop_kPa: "psets.BIP.PressureDrop_kPa",
  PressureDrop_Pa: "psets.BIP.PressureDrop_Pa",
  ProductCode: "psets.BIP.ProductCode",
  ProductCodeProcured: "psets.BIP.ProductCodeProcured",
  ProductSize: "psets.BIP.ProductSize",
  ProductType: "psets.BIP.ProductType",
  ptot_kPa: "psets.BIP.ptot_kPa",
  ptot_mbar: "psets.BIP.ptot_mbar",
  ptot_Pa: "psets.BIP.ptot_Pa",
  RDSCode: "psets.BIP.RDSCode",
  RoughHeight: "psets.BIP.RoughHeight",
  RoughWidth: "psets.BIP.RoughWidth",
  RunningIndex: "psets.BIP.RunningIndex",
  SiteFMGUID: "psets.BIP.SiteFMGUID",
  Slope: "psets.BIP.Slope",
  SoundLevel_dBA: "psets.BIP.SoundLevel_dBA",
  SpaceName: "psets.BIP.SpaceName",
  SpaceNumber: "psets.BIP.SpaceNumber",
  SpaceType: "psets.BIP.SpaceType",
  StartElevation: "psets.BIP.StartElevation",
  StatusConstruction: "psets.BIP.StatusConstruction",
  StatusObject: "psets.BIP.StatusObject",
  StoreyName: "psets.BIP.StoreyName",
  SystemCode: "psets.BIP.SystemCode",
  SystemID: "psets.BIP.SystemID",
  SystemName: "psets.BIP.SystemName",
  SystemType: "psets.BIP.SystemType",
  TopElevation: "psets.BIP.TopElevation",
  TopElevationAbs: "psets.BIP.TopElevationAbs",
  Type: "psets.BIP.Type",
  TypeDescription: "psets.BIP.TypeDescription",
  TypeID: "psets.BIP.TypeID",
  TypeLongDescription: "psets.BIP.TypeLongDescription",
  TypeName: "psets.BIP.TypeName",
  UsableFloorArea: "psets.BIP.UsableFloorArea",
  ["Velocity_m/s"]: "psets.BIP.Velocity_m/s",
  Voltage_V: "psets.BIP.Voltage_V",
  Volume: "psets.BIP.Volume",
  Weight: "psets.BIP.Weight",
  Width: "psets.BIP.Width"
}

export const GLOBAL_ATTR = {
  globalId: "globalId",
  name: "name",
  instanceClass: "instanceClass",
  typeClass: "type.typeClass",
}

const ALL_ATTRIBUTES: { [key: string]: string } = {
  ...BIP_PROPERTIES,
  ...GLOBAL_ATTR
};

const AG_TABLE_COLUMNS: ColDef[] = Object.keys(ALL_ATTRIBUTES).map(key => {
  return {
    headerName: key,
    field: ALL_ATTRIBUTES[key],
    hide: true
  }
});

const OTHER_COLUMNS:ColDef[] = [
  {
    headerName: "Höjdintervall",
    field: "heightInterval"
  },
  {
    headerName: "", // Sätts beroende på vilken egenskap som summeras
    field: "sumValue",
  //valueFormatter: (params) => new Intl.NumberFormat('se-SE', { maximumFractionDigits: 0 }).format(params.value/1000)
  },
  {
    headerName: "Σ Antal",
    field: "count"
  }
]

export const TABLE_COLUMNS:ColDef[] = [...AG_TABLE_COLUMNS, ...OTHER_COLUMNS].map(col => {
  const defaultActiveColumns = ["psets.BIP.TypeID", "type.typeClass"]
  col.sortable = true;
  col.filter = true;
  col.hide = !defaultActiveColumns.includes(col.field as string);
  return col;
})
