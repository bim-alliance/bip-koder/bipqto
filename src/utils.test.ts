import { describe, expect } from 'vitest';
import { IBimItem, IExtractionConfig } from './types';
import { z } from "zod";
import { getGroupKeyForBimItem, getHeightIntervalForBimItem, groupBimItems } from './utils';

describe('utils', () => {
  it('fn getHeightIntervalForBimItem', () => {
    let bimItem: IBimItem = {
      fileName: 'test',
      expressId: 1,
      globalId: 'test',
      instanceClass: 'test',
      name: 'test',
      psets: {
        BIP: {
          CenterOfElevation: 2950
        }
      },
      type: null,
      zodErrors: null,
      missingAttributes: null,
    };
    let extractionConfig:IExtractionConfig = {
      name: 'test',
      includedIfcClasses: [],
      excludedIfcClasses: [],
      requirements: z.object({}),
      groupBy: {
        properties: [],
        sumType: 'COUNT',
        sumProperty: 'psets.BIP.Length',
        heightProperty: 'psets.BIP.CenterOfElevation',
        heightSumInterval: [[0, 3000], [3000, 6000], [6000, null]]
      },
    };
    expect(getHeightIntervalForBimItem(bimItem, extractionConfig)).toStrictEqual([0, 3000]);

    let bimItem2: IBimItem = {
      fileName: 'test',
      expressId: 1,
      globalId: 'test',
      instanceClass: 'test',
      name: 'test',
      psets: {
        BIP: {
          CenterOfElevation: 7000
        }
      },
      type: null,
      zodErrors: null,
      missingAttributes: null
    };

    expect(getHeightIntervalForBimItem(bimItem2, extractionConfig)).toStrictEqual([6000, null]);
  });

  it("fn getGroupKeyForBimItem", () => {
    let bimItem: IBimItem = {
      fileName: 'test',
      expressId: 1,
      globalId: 'test',
      instanceClass: 'IfcFlowController',
      name: 'test',
      psets: {
        BIP: {
          TypeID: 'TD601',
          CenterOfElevation: 3100
        }
      },
      type: null,
      zodErrors: null,
      missingAttributes: null,
    };

    let extractionConfig: IExtractionConfig = {
        name: "VS Styckvaror",
        includedIfcClasses: [],
        excludedIfcClasses: ["IFCFLOWSEGMENT", "IFCFLOWFITTING", "IFCCOVERING"],
        requirements: z.object({
          "globalId": z.string().min(1),
          "instanceClass": z.string().min(1),
          psets: z.object({
            BIP: z.object({
              TypeID: z.string().min(1),
            }),
          }),
        }),
        groupBy: {
          properties:  ["instanceClass", "psets.BIP.TypeID"],
          sumType: "COUNT"
        }
      };

    expect(getGroupKeyForBimItem(bimItem, extractionConfig)).toStrictEqual("IfcFlowController;TD601");

    let bimItem2: IBimItem = {
      fileName: 'test',
      expressId: 1,
      globalId: 'test',
      instanceClass: 'IfcFlowController',
      name: 'test',
      psets: {
        BIP: {
          TypeID: 'TD601',
          CenterElevation: 3100
        }
      },
      type: null,
      zodErrors: null,
      missingAttributes: null,
    };

    let extractionConfig2: IExtractionConfig = {
      name: "VS Längdvaror",
      includedIfcClasses: ["IFCFLOWSEGMENT"],
      excludedIfcClasses: ["IFCFLOWFITTING"],
      requirements: z.object({
        "globalId": z.string().min(1),
        psets: z.object({
          BIP: z.object({
            CenterElevation: z.number().gte(-1000),
          }),
        }),
      }),
      groupBy: {
        properties:  ["instanceClass", "psets.BIP.TypeID"],
        sumType: "SUM",
        sumProperty: "psets.BIP.Length",
        heightProperty: "psets.BIP.CenterElevation",
        heightSumInterval: [[0,1799], [1800, 3000], [3001, 4500], [4501, 7000], [7000, null]]
      }
    };

    expect(getGroupKeyForBimItem(bimItem2, extractionConfig2)).toStrictEqual("IfcFlowController;TD601;3001-4500");
  });

  it ('fn groupBimItems', () => {
    let bimItems: IBimItem[] = [
      {
        fileName: 'test',
        expressId: 1,
        globalId: 'test',
        instanceClass: 'IfcFlowController',
        name: 'test',
        psets: {
          BIP: {
            TypeID: 'TD601',
            CenterOfElevation: 3100
          }
        },
        type: null,
        zodErrors: null,
        missingAttributes: null,
      },
      {
        fileName: 'test',
        expressId: 1,
        globalId: 'test',
        instanceClass: 'IfcFlowController',
        name: 'test',
        psets: {
          BIP: {
            TypeID: 'TD601',
            CenterOfElevation: 3100
          }
        },
        type: null,
        zodErrors: null,
        missingAttributes: null,
      }
    ];

    let extractionConfig: IExtractionConfig = {
      name: "VS Styckvaror",
      includedIfcClasses: [],
      excludedIfcClasses: ["IFCFLOWSEGMENT", "IFCFLOWFITTING", "IFCCOVERING"],
      requirements: z.object({
        "globalId": z.string().min(1),
        "instanceClass": z.string().min(1),
        psets: z.object({
          BIP: z.object({
            TypeID: z.string().min(1),
          }),
        }),
      }),
      groupBy: {
        properties:  ["instanceClass", "psets.BIP.TypeID"],
        sumType: "COUNT"
      }
    };

    expect(groupBimItems(bimItems, extractionConfig)).toStrictEqual([{
      "count": 2,
      "heightInterval": "N/A",
      "sumValue": 0,
      "groupKey": "IfcFlowController;TD601",
      "groupedProperties": {
        "instanceClass": "IfcFlowController",
        psets: {
          BIP: {
            TypeID: "TD601"
          }
        }
      },
    }]);
  });
});

