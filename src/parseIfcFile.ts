import type * as WebIfc from 'web-ifc';
import { IFCRELDEFINESBYPROPERTIES, IFCRELDEFINESBYTYPE } from 'web-ifc';
import type * as IfcBaseEntity from './webIfcTypes';
import type { IWebIfcPset } from './webIfcTypes';
import type { IBimItem, IExtractionConfig, IIfcType, INumberArrayMap, IPropertyOrQuantitySet } from './types';
import { EXCLUDED_IFC_CLASSES } from './constants';
import {
  getPropertySetsFromHashmap,
  getRelatingTypeFromHashmap,
  getRelationsWithRelatingObjectHashmap
} from './ifcParserUtils';
import { getMissingAttributesInBimItem, getZodErrorsInBimItem } from './utils';

interface IArgs {
  ifcApi: WebIfc.IfcAPI;
  buffer: Uint8Array;
  fileName: string;
  profileOrBipProperties:  "PROFILE" | "BIP_PROPERTIES"
  extractionConfig: IExtractionConfig | null; // Används när profil anävnds
  attributesConfig: string[] | null; // "psets.BIP.TypeID" // Används när egna kolumner används
}

export const getBimItemsFromIfcFile = async (args: IArgs) => {

  let modelID = args.ifcApi.OpenModel(args.buffer);
  const lines = args.ifcApi.GetAllLines(modelID);
  const bimItems: IBimItem[] = [];

  const lineSize = lines.size();


  const ifcReldefinesByPropertyLinesHashmap = getRelationsWithRelatingObjectHashmap({
    client: args.ifcApi,
    ifcConstant: IFCRELDEFINESBYPROPERTIES,
    itemKey: 'RelatingPropertyDefinition',
    objectKey: 'RelatedObjects',
  });

  const ifcRelTypesHashamp = getRelationsWithRelatingObjectHashmap({
    client: args.ifcApi,
    ifcConstant: IFCRELDEFINESBYTYPE,
    itemKey: 'RelatingType',
    objectKey: 'RelatedObjects',
  });

  console.log("lineSize: ", lineSize);


  for (let i = 1; i <= lineSize; i++) {
    try {
      const itemId = lines.get(i);
      if (itemId == null) {
        continue;
      }
      let row: IfcBaseEntity.IRootObject;

      try {
        row = args.ifcApi.GetLine(modelID, itemId);
      } catch (err) {
       // console.log('IFCJS: GetLine error')
        //console.log(`itemId: ${itemId}`)
        continue;
      }
      const isIfcElement = args.ifcApi.IsIfcElement(row.type);
      const instanceClass = row.__proto__.constructor.name;
      if (!isIfcElement || EXCLUDED_IFC_CLASSES.includes(instanceClass)) {
        continue;
      }
      let bimItem = await convertIfcRowToBimItem(args.ifcApi, row, args.fileName, ifcReldefinesByPropertyLinesHashmap, ifcRelTypesHashamp);
      if (args.extractionConfig != null && args.profileOrBipProperties === "PROFILE") {
        bimItem.zodErrors = getZodErrorsInBimItem(bimItem, args.extractionConfig.requirements);
      }
      if (args.profileOrBipProperties === "BIP_PROPERTIES" && args.attributesConfig != null) {
        const missingAttributes = getMissingAttributesInBimItem(bimItem, args.attributesConfig);
        bimItem.missingAttributes = missingAttributes.length > 0 ? missingAttributes : null;
      }
      bimItems.push(bimItem);
    } catch (err) {
     // console.log('IFCJS: GetAllLines error');
     // console.log(err);
    }
  }
  args.ifcApi.CloseModel(modelID);
  return bimItems;
}

const convertIfcRowToBimItem = async (ifcApi: WebIfc.IfcAPI, row: IfcBaseEntity.IRootObject, fileName: string, ifcReldefinesByPropertyLinesHashmap: INumberArrayMap | undefined, ifcRelTypesHashamp: INumberArrayMap | undefined):Promise<IBimItem> => {

  const instancePsets = getPropertySetsFromHashmap(ifcApi, row.expressID, ifcReldefinesByPropertyLinesHashmap);
  const relType = getRelatingTypeFromHashmap(ifcApi, row.expressID, ifcRelTypesHashamp);

  const type = () => {
    if (!relType) return null;
    const { psets, ...rest } = relType;
    return rest;
  };

  const allPsets = relType?.psets != null ? mergePsets(relType.psets, instancePsets) : instancePsets;

  return {
    expressId: row.expressID,
    globalId: row.GlobalId.value,
    instanceClass: row.__proto__.constructor.name,
    psets: allPsets,
    type: type(),
    name: row.Name?.value ?? '',
    fileName,
    zodErrors: null,
    missingAttributes: null,
  }
}

export const mergePsets = (
  typePset: IPropertyOrQuantitySet,
  instancePset: IPropertyOrQuantitySet
): IPropertyOrQuantitySet => {
  const mergedPset: IPropertyOrQuantitySet = { ...typePset };

  Object.keys(instancePset).forEach(key => {
    if (mergedPset[key] && typeof instancePset[key] === 'object' && !Array.isArray(instancePset[key])) {
      mergedPset[key] = { ...mergedPset[key], ...instancePset[key] };
    } else {
      mergedPset[key] = instancePset[key];
    }
  });

  return mergedPset;
};


// Dessa är funktionerna från web-ifc för att plocka ut psets och type. De är extremt långsamma. Något fel i nedanstående kod eller behöver web-ifc optimera? Tills det är fixat så använder vi våra egna funktioner.
const getPsetsNotUse = async (ifcApi: WebIfc.IfcAPI, expressId: number):Promise<IPropertyOrQuantitySet> => {

  let webIfcPsets = await ifcApi.properties.getPropertySets(0, expressId) as unknown as IWebIfcPset[];
  return webIfcPsets.reduce((acc, cur) => {
    const psetName = cur.Name?.value;
    if (!psetName) return acc;
    for (const prop of cur.HasProperties) {
      const property = ifcApi.GetLine(0, prop.value);
      const propName = property.Name?.value;
      const propValue = property.NominalValue?.value;
      if (propName && propValue) {
        acc[psetName] = { ...acc[psetName], [propName]: propValue };
      }
    }
    return acc;
  }, {} as IPropertyOrQuantitySet);
}

export const getTypeNotUse = async (ifcApi: WebIfc.IfcAPI, expressId: number):Promise<IIfcType | null> => {
  const webIfcTypes = await ifcApi.properties.getTypeProperties(0, expressId) as unknown as IfcBaseEntity.IRootObject[];
  if (webIfcTypes == null) {
    return null;
  }
  if (webIfcTypes[0] == null) {
    return null;
  }
  const webIfcType = webIfcTypes[0];
  const typeClass = webIfcType.__proto__.constructor.name;
  //const psets = await getPsets(ifcApi, webIfcType.expressID);
  return {
    globalId: webIfcType.GlobalId.value,
    name: webIfcType.Name?.value ?? '',
    typeClass,
    expressId: webIfcType.expressID,
  };
}
