
export type IProperty = {
  property: string;
  value: string | number;
};

export interface IValueType {
  value: number;
  type: number
}

export type IGlobalId = {
  type: number;
  value: string;
};

export type IOwnerHistory = {
  type: number;
  value: number;
};

export type IName = {
  type: number;
  value: string;
};

export type IObjectType = {
  type: number;
  value: string;
};

export type IObjectPlacement = {
  type: number;
  value: number;
};

export type IRepresentation = {
  type: number;
  value: number;
};

export type ITag = {
  type: number;
  value: string;
};

export type ILongName = {
  type: number;
  value: string;
};



export type IRootObject = {
  HasPropertySets?: { value: number | null }[];
  __proto__: {
    constructor: {
      name: string;
    };
  };
  expressID: number;
  type: number;
  GlobalId: IGlobalId;
  OwnerHistory: IOwnerHistory;
  Name?: IName;
  Description?: string;
  ObjectType: IObjectType;
  ObjectPlacement: IObjectPlacement;
  Representation: IRepresentation;
  Elevation?: { value: number };
  Tag: ITag;
  ifcInstanceClass: string;
  HasProperties?: { value: number }[];
  Quantities?: { value: number }[];
  LongName?: ILongName;
};

export interface IWebIfcPset {
  Name: {
    value: string; // BIP
  },
  HasProperties: IValueType[]
}

export interface IWebIfcType {
  expressID: number;
  Name: {
    value: string;
  }
}
