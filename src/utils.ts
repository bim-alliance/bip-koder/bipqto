import { z, ZodFormattedError, ZodObject } from "zod"
import type { IBimItem, IExtractionConfig, IGroupedItem } from "./types"
import { utils, writeFile } from "xlsx"
import type { ColDef } from "ag-grid-community"

export const getZodErrorsInBimItem = (
  bimItem: IBimItem,
  zodSchema: z.ZodObject<any, any>,
): ZodFormattedError<IBimItem> | null => {
  const result = zodSchema.safeParse(bimItem)
  if (!result.success) {
    return result.error.format()
  }
  return null
}

export const getMissingAttributesInBimItem = (bimItem: IBimItem, attributes: string[]): string[] => {
  return attributes.filter((attr) => {
    return getDeeplyNestedValue(bimItem, attr) == null;
  });
}

export const getGroupdAgGridColumnDefs = (extractionConfig: IExtractionConfig) => {
  const attriebutCols = extractionConfig.groupBy.properties.map((prop) => {
    return {
      field: `groupedProperties.${prop}`,
      sortable: true,
      filter: true,
      headerName: prop,
      valueFormatter: (params: any) => {
        return params.data.value
      }
    }
  });
  if (extractionConfig.groupBy.heightSumInterval != null) {
    attriebutCols.push({
      field: "heightInterval",
      sortable: true,
      filter: true,
      headerName: "Höjdintervall",
      valueFormatter: (params) => {
        return params.value
      }
    });
  }
  if (extractionConfig.groupBy.sumProperty != null) {
    attriebutCols.push({
      field: "sumValue",
      sortable: true,
      filter: true,
      headerName: `Σ ${extractionConfig.groupBy.sumProperty} / 1000`,
      valueFormatter: (params) => new Intl.NumberFormat('se-SE', { maximumFractionDigits: 0 }).format(params.value/1000)
    });
  }
  attriebutCols.push({
    field: "count",
    sortable: true,
    filter: true,
    headerName: "Σ Antal",
    valueFormatter: params => params.value
  });
  return attriebutCols;
}


export const getDeeplyNestedValue = <T, TValue>(obj: T, path: string): TValue | null => {
  const parts = path.split(".");
  let result: any = obj;

  for (let i = 0; i < parts.length; i++) {
    if (result == null) {
      return null;
    }

    result = result[parts[i]];
  }

  return result;
}


export const extractZodSchemaPaths = (schema: any, parentPath: string = ''): string[] => {
  if (!(schema instanceof ZodObject)) {
    return [];
  }
  let paths: string[] = [];
  for (const key in schema.shape) {
    const fullPath = parentPath ? `${parentPath}.${key}` : key;
    const fieldSchema = schema.shape[key];
    if (fieldSchema instanceof ZodObject) {
      paths = paths.concat(extractZodSchemaPaths(fieldSchema, fullPath));
    } else {
      paths.push(fullPath);
    }
  }
  return paths;
}

export const getHeightIntervalForBimItem = (bimItem: IBimItem, extractionConfig: IExtractionConfig): [number,number | null] | null => {
  if (extractionConfig.groupBy.heightSumInterval == null || extractionConfig.groupBy.heightProperty == null) {
    return null;
  }
  const height = getDeeplyNestedValue(bimItem, extractionConfig.groupBy.heightProperty);
  if (height == null || typeof height !== 'number') {
    return null;
  }
  for (const interval of extractionConfig.groupBy.heightSumInterval) {
    if (interval[1] == null && height >= interval[0]) {
      return interval;
    }
    if (height >= interval[0]) {
      if (interval[1] == null) {
        return interval;
      }
      if (height < interval[1]) {
        return interval;
      }
    }
  }
  return null;
}

export const getGroupKeyForBimItem = (bimItem: IBimItem, extractionConfig: IExtractionConfig): string => {
  const groupByProperties = extractionConfig.groupBy.properties;
  let groupKey = groupByProperties.map((prop: string) => {
    const value = getDeeplyNestedValue(bimItem, prop);
    if (value != null) {
      return value;
    }
    return '__SAKNAS__'; // or some fallback logic
  }).join(";");
  const heightInterval = getHeightIntervalForBimItem(bimItem, extractionConfig);

  if (heightInterval != null) {
    const heightIntervalKey = heightInterval.map((val) => {
      if (val == null) {
        return '∞';
      }
      return val.toString();
    });
    if (heightIntervalKey != null) {
      groupKey = `${groupKey};${heightIntervalKey.join("-")}`;
    }
  }
  return groupKey;
}

export const groupBimItems = (bimItems: IBimItem[], extractionConfig: IExtractionConfig): IGroupedItem[] => {
  // Ensure acc is correctly typed as an object with string keys and IGroupedItem values
  const groupedItems = bimItems.reduce((acc: Record<string, IGroupedItem>, item) => {
    const groupKey = getGroupKeyForBimItem(item, extractionConfig);

    const heightInterval = getHeightIntervalForBimItem(item, extractionConfig);
    const heightIntervalAsString = heightInterval != null ? heightInterval.toString() : "N/A";
    if (!acc[groupKey]) {
      acc[groupKey] = {
        count: 0,
        sumValue: 0,
        heightInterval: heightIntervalAsString,
        groupKey: groupKey,
        groupedProperties: {
          ...extractionConfig.groupBy.properties.reduce((acc2, prop) => {
            setDeeplyNestedValue(acc2, prop, getDeeplyNestedValue(item, prop) ?? "");
            return acc2;
          }, {} as Record<string, any>)
        }
      }
    }
    acc[groupKey].count++;
    if (extractionConfig.groupBy.sumProperty != null) {
      const sumValue = getDeeplyNestedValue(item, extractionConfig.groupBy.sumProperty);
      if (sumValue != null && typeof sumValue === 'number') {
        acc[groupKey].sumValue = acc[groupKey].sumValue + sumValue;
      }
    }
    return acc;
  }, {} as Record<string, IGroupedItem>);
  return Object.values(groupedItems);
}

const setDeeplyNestedValue = (obj: Record<string, any>, path: string, value: any) => {
  const parts = path.split(".");
  let current = obj;

  for (let i = 0; i < parts.length - 1; i++) {
    if (!current[parts[i]]) {
      current[parts[i]] = {};
    }
    current = current[parts[i]];
  }

  current[parts[parts.length - 1]] = value;
};

export const bimItemIsIncludedInFilter = (bimItem: IBimItem, extractionConfig: IExtractionConfig): boolean => {
  if (extractionConfig.excludedIfcClasses.includes(bimItem.instanceClass?.toUpperCase())) {
    return false;
  }

  return extractionConfig.includedIfcClasses.length === 0 || extractionConfig.includedIfcClasses.includes(bimItem.instanceClass?.toUpperCase());

}

export const getPrpertiesFromZodObject = (zodObject: ZodObject<any, any>, parentPath: string = ''): string[] => {
  let paths: string[] = [];
  for (const key in zodObject.shape) {
    const fullPath = parentPath ? `${parentPath}.${key}` : key;
    const fieldSchema = zodObject.shape[key];
    if (fieldSchema instanceof ZodObject) {
      paths = paths.concat(getPrpertiesFromZodObject(fieldSchema, fullPath));
    } else {
      paths.push(fullPath);
    }
  }
  return paths;
}

export const exportExcel = (columns: ColDef[], arr: any[]) => {
  const columnsToExport = columns.filter((col) => col.hide !== true && col.field != null);
  const rows = getAgGridRows(columns, arr);
  const excelData = [columnsToExport?.map((col) => col.headerName), ...rows];

  // Ensure all data is in a simple format
  const sanitizedData = excelData.map(row =>
    row.map(cell => (typeof cell === 'object' ? JSON.stringify(cell) : cell))
  );

  const ws = utils.aoa_to_sheet(sanitizedData);
  const wb = utils.book_new();
  utils.book_append_sheet(wb, ws, "Data");
  writeFile(wb, "data.xlsx");
}

export const getAgGridRows = (columns: ColDef[], rows: any[]): any[][] => {
  return rows.reduce((acc, row) => {
    const attr: any[] = [];
    columns?.forEach((column) => {
      if (column.hide !== true && column.field) {
        attr.push(getDeeplyNestedValue(row, column.field));
      }
    });
    acc.push(attr);
    return acc;
  }, []);
};
